#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <ctype.h>
#include "tm_stm32f4_mfrc522.h"

static  uint8_t m_CardID[5];
static  uint8_t m_buffer_recive[16];

static tag_read_evt_handler_t m_tag_read_evt_handler = NULL;
monte_err tag_read_read_block(uint8_t *CardID, uint8_t block_addr,uint8_t* buffer_recive){
	int ret, i;
	uint8_t buffer[30] = "";
	uint8_t SectorKeyA[6] = { 0x19, 0x09, 0x5F, 0x4b, 0x44, 0x4d };// { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };

	uint8_t *SectorKey;
	SectorKey = SectorKeyA;

	debug_info(
			"Auth Block (0x%02X) with key 0x%02X 0x%02X 0x%02X 0x%02X 0x%02X ...",
			block_addr, SectorKey[0], SectorKey[1], SectorKey[2], SectorKey[3],
			SectorKey[4]);

	ret = TM_MFRC522_Auth((uint8_t) PICC_AUTHENT1A, (uint8_t) block_addr,
			(uint8_t*) SectorKey, (uint8_t*) CardID);
	if (ret == MI_OK) {
		debug_info("OK");
                TM_MFRC522_Read(4, buffer);
                memcpy(buffer_recive,buffer,16);
//		NRF_LOG_HEXDUMP_INFO(buffer,16);
		return 0;

	} else {
		debug_info("Failed\r\n");
		return -1;
	}
}
void tag_read_set_handler(tag_read_evt_handler_t read_handle){
  m_tag_read_evt_handler = read_handle;
}
void tag_read_process(){
     // Reset rx buffer and transfer done flag
     if (!(TM_MFRC522_Check(m_CardID) == MI_OK)) {
//////        nrf_delay_ms(100);
        return;
     }

     if(!tag_select(m_CardID)== MI_OK){
//////        nrf_delay_ms(100);
        return;
     }

//////      NRF_LOG_INFO("tag_select OK:%i",(int)my_timer_get_ms());
//      NRF_LOG_HEXDUMP_INFO(m_CardID,5);
//////      TM_MFRC522_Debug_DumpSector(m_CardID,4);
      memset(m_buffer_recive,0,16);
      tag_read_read_block(m_CardID,4,m_buffer_recive);
      
      if(m_tag_read_evt_handler!=NULL){
        m_tag_read_evt_handler(m_buffer_recive);
      }
      PCD_StopCrypto1();
      TM_MFRC522_Halt();
//////      NRF_LOG_INFO("DumpSector:%i",(int)my_timer_get_ms());
      int k=0;
      for(int i = 0; i < 1000 ; i++){
          if(TM_MFRC522_Check(m_CardID) == MI_OK){
                PCD_StopCrypto1();
                TM_MFRC522_Halt();
//                NRF_LOG_INFO("%d",k++);                
          } else {
          PCD_StopCrypto1();
          TM_MFRC522_Halt();
          m_tag_read_evt_handler(NULL);//hoan thinh doc bloak, co th e nau nay dung them cai hadle_id          
          break;
          }
      }
}