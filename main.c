/**
 * Copyright (c) 2014 - 2019, Nordic Semiconductor ASA
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 *
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 *
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
/**
 * @brief BLE Heart Rate and Running Speed relay application main file.
 *
 * @detail This application demonstrates a simple relay, which passes on the values it receives.
 * By combining a collector part on one end and a sensor part on the other,
 * the s130 can function simultaneously as a central and a peripheral device.
 * 
 * The following figure shows how the sensor ble_app_hrs connects and interacts with the relay
 * in the same manner it would connect to a Heart Rate Collector. In this case, the relay
 * application acts as a central.
 *
 * On the other side, a collector (such as Master Control Panel or ble_app_hrs_c) connects
 * and interacts with the relay in the same manner it would connect to a heart rate sensor peripheral.
 *
 * LED layout:
 * LED 1: Central side is scanning.       LED 2: Central side is connected to a peripheral.
 * LED 3: Peripheral side is advertising. LED 4: Peripheral side is connected to a central.
 *
 * @note While testing, make sure that the Sensor and Collector are actually connecting to the relay,
 *       and not directly to each other!
 *
 *    Peripheral                  Relay                    Central
 *    +--------+        +-----------|----------+        +-----------+
 *    | Heart  |        | Heart     |   Heart  |        |           |
 *    | Rate   | -----> | Rate     -|-> Rate   | -----> | Collector |
 *    | Sensor |        | Collector |   Sensor |        |           |
 *    +--------+        +-----------|   and    |        +-----------+
 *                      | Running   |   Running|
 *    +--------+        | Speed    -|-> Speed  |
 *    | Running|------> | Collector |   Sensor |
 *    | Speed  |        +-----------|----------+
 *    | Sensor |
 *    +--------+
 */

#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "app_mpu.h"
#include "nrf_drv_twi.h"
#include "tm_stm32f4_mfrc522.h"
#include "cmd_mgmt.h"
#include "nrf_ws2812.h"
#include "mem_manager.h"
#include "buzzer.h"
#include "motor.h"

#include "nrf_drv_gpiote.h" //Nho enable NRFX_GPIO in sdk
#include "nrf_drv_saadc.h"

#include "system.h"
#include "nrf_pwr_mgmt.h"
static uint16_t              m_adc_evt_counter; //couter adc
static uint16_t               m_nfc_adc_evt_counter;//couter evt nfc -> adc
#define SAMPLES_IN_BUFFER 5
static nrf_saadc_value_t     m_buffer_pool[2][SAMPLES_IN_BUFFER];

volatile bool is_IRQ_mpu;
volatile bool tag_read_process_allow;

#define   PIN_IRQ_MPU 05
#define   MFRC522_POWER_PIN 26

extern buzzer_t tone_connected;


void in_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
    is_IRQ_mpu =true;
}
const nrf_drv_timer_t TIMER_LED = NRF_DRV_TIMER_INSTANCE(2);
//APP_TIMER_DEF(m_repeated_timer_nfc);     /**< Handler for repeated timer used to blink LED 1. */
static void repeated_timer_nrf_handler(void * p_context)
{
//buzzer_run(NOTE_C,100);
//tag_read_process_allow= true;
}
void timer_nfc_read_event_handler(nrf_timer_event_t event_type, void* p_context)
{
    switch (event_type)
    {
        case NRF_TIMER_EVENT_COMPARE0:{
        //doc adc battery
            m_nfc_adc_evt_counter++;
            if(m_nfc_adc_evt_counter==10) { //100ms*20/2=10s
              m_nfc_adc_evt_counter = 0;
              uint32_t err_code = nrf_drv_saadc_sample();
              APP_ERROR_CHECK(err_code);
            }           
            //triger read nfc
            tag_read_process_allow= true;
           }
            break;
        default:
            //Do nothing.
            break;
    }
}
void mpu6050_init(){
  ret_code_t err_code;
  err_code = app_mpu_init();
  APP_ERROR_CHECK(err_code);
  //Install interup gpio ,handle IRQ from MPU6050
  if(!nrf_drv_gpiote_is_init()){
      err_code = nrf_drv_gpiote_init();
  }
  nrf_drv_gpiote_in_config_t in_config = GPIOTE_CONFIG_IN_SENSE_TOGGLE(true);
  in_config.pull = NRF_GPIO_PIN_PULLUP;
  in_config.sense = NRF_GPIOTE_POLARITY_HITOLO;

  err_code = nrf_drv_gpiote_in_init(PIN_IRQ_MPU, &in_config, in_pin_handler);
  NRF_LOG_INFO("err_code:%d",err_code);
  APP_ERROR_CHECK(err_code);

  nrf_drv_gpiote_in_event_enable(PIN_IRQ_MPU, true);





}
void cmd_block_piano_handler(void * p_context){
  note_t  * _note = (note_t*) p_context;
  buzzer_piano_start(*_note);
}
void cmd_block_handler(cmd_evt_t const * p_cmd_evt, void * p_context){
  switch(*p_cmd_evt){
    case CMD_ON_BLOCK_BEGIN:
      NRF_LOG_INFO("CMD_ON_BLOCK_BEGIN");
      break;
    case CMD_ON_BLOCK_END:
        NRF_LOG_INFO("CMD_ON_BLOCK_END");
        break;
    case CMD_ON_BLOCK_END_LOOP:
        NRF_LOG_INFO("CMD_ON_BLOCK_END_LOOP");
        break;
    case CMD_ON_BLOCK_MFORWARD:
          NRF_LOG_INFO("CMD_ON_BLOCK_MFORWARD");
      break;
    case CMD_ON_BLOCK_MBACKWARD:
          NRF_LOG_INFO("CMD_ON_BLOCK_MBACKWARD");
      break;
    case CMD_ON_BLOCK_TURNLEFT:
          NRF_LOG_INFO("CMD_ON_BLOCK_TURNLEFT");
      break;
    case CMD_ON_BLOCK_TURNRIGHT:
          NRF_LOG_INFO("CMD_ON_BLOCK_TURNRIGHT");
      break;
    case CMD_START_BLOCK_PIANO:{
          NRF_LOG_INFO("CMD_START_BLOCK_PIANO");
          cmd_block_piano_handler(p_context);
          return;
          }
      break;
    case CMD_FINISH_BLOCK_PIANO:{
          NRF_LOG_INFO("CMD_FINISH_BLOCK_PIANO");
          buzzer_piano_stop();
          return;
          }
      break;
      case CMD_ON_COMPLETED_PROGRAM:
      NRF_LOG_INFO("CMD_ON_COMPLETED_PROGRAM");
      break;
      case CMD_FINISH_BLOCK:
      NRF_LOG_INFO("CMD_FINISH_BLOCK");
      return;
      break;
    default:
          NRF_LOG_INFO("CMD_ON_BLOCK_UNKNOW");
      break;
  }
  if((*p_cmd_evt != CMD_START_BLOCK_PIANO) || (*p_cmd_evt != CMD_FINISH_BLOCK_PIANO)) buzzer_run(NOTE_C,100);
  motot_run(100);
  cmd_evt_t evt;
  //neu chau connected ble
  if(ble_conn_state_central_conn_count()==0) {
    if(*p_cmd_evt==CMD_ON_COMPLETED_PROGRAM){
        evt = CMD_ON_BLOCK_END; //sau nay dugn p_contex de biet end hay endloop
    } else {
       evt = *p_cmd_evt;
    }
    ws_device_show(PRESENT_CMD,&evt);
  } else {
  if(*p_cmd_evt==CMD_ON_COMPLETED_PROGRAM){
    evt = CMD_ON_COMPLETED_PROGRAM;
    ws_device_show(PROGRAM_STT,&evt);
  } else{
    ws_device_show(RESET_LED,NULL);
    ws_device_show(PRESENT_CMD,p_cmd_evt);
    }
  }
}
void mfrc522_read_tag_handler(uint8_t* buffer_recive){
    NRF_LOG_INFO("mfrc522 raw data:");
    if(buffer_recive!=0) NRF_LOG_HEXDUMP_INFO(buffer_recive,16);
    //tao cai handler cho cmd process-> end start full....
    cmd_mgmt_take_tag(buffer_recive);

}
void MFRC522_Init(){
    nrf_gpio_cfg_output(MFRC522_POWER_PIN);
    nrf_gpio_pin_clear(MFRC522_POWER_PIN);
    nrf_delay_ms(50);
    TM_MFRC522_Init();
    tag_read_set_handler(mfrc522_read_tag_handler);
      /*setup timer read nfc*/

    // Create timers
//    err_code = app_timer_create(&m_repeated_timer_nfc,
//                                APP_TIMER_MODE_REPEATED,
//                                repeated_timer_nrf_handler);
//    APP_ERROR_CHECK(err_code);
//    err_code = app_timer_start(m_repeated_timer_nfc, APP_TIMER_TICKS(500), NULL);
//    APP_ERROR_CHECK(err_code);



    uint32_t time_read_ms = 100; //Time(in miliseconds) between consecutive compare events.
    uint32_t time_ticks;
    uint32_t err_code = NRF_SUCCESS;
    //Configure TIMER_LED for generating simple light effect - leds on board will invert his state one after the other.
    nrf_drv_timer_config_t timer_cfg = NRF_DRV_TIMER_DEFAULT_CONFIG;
    timer_cfg.frequency = NRF_TIMER_FREQ_31250Hz;
    timer_cfg.bit_width = NRF_TIMER_BIT_WIDTH_16;
    err_code = nrf_drv_timer_init(&TIMER_LED, &timer_cfg, timer_nfc_read_event_handler);
    APP_ERROR_CHECK(err_code);

    time_ticks = nrf_drv_timer_ms_to_ticks(&TIMER_LED, time_read_ms);
    NRF_LOG_INFO("time_ticks:%d",time_ticks);
    nrf_drv_timer_extended_compare(
         &TIMER_LED, NRF_TIMER_CC_CHANNEL0, time_ticks, NRF_TIMER_SHORT_COMPARE0_CLEAR_MASK, true);

    nrf_drv_timer_enable(&TIMER_LED);
}
void cmd_mgmt_init(){
    nrf_mem_init();//install memory :calloc malloc
    cmd_init(cmd_block_handler);
}

//callback when data is full
void saadc_callback(nrf_drv_saadc_evt_t const * p_event)
{
    if (p_event->type == NRF_DRV_SAADC_EVT_DONE)
    {
        ret_code_t err_code;
        err_code = nrf_drv_saadc_buffer_convert(p_event->data.done.p_buffer, SAMPLES_IN_BUFFER);
        APP_ERROR_CHECK(err_code);

        int i;
//        NRF_LOG_INFO("ADC event number: %d", (int)m_adc_evt_counter);
        int tb =0;
        for (i = 0; i < SAMPLES_IN_BUFFER; i++)
        {
            tb += p_event->data.done.p_buffer[i];
//            NRF_LOG_INFO("%d", p_event->data.done.p_buffer[i]);
        }
//        NRF_LOG_INFO("Battery:%d",tb/SAMPLES_IN_BUFFER);
        m_adc_evt_counter++;
    }
}
void saadc_init(void)
{
    ret_code_t err_code;
    nrf_saadc_channel_config_t channel_config =
        NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(NRF_SAADC_INPUT_AIN0);

    err_code = nrf_drv_saadc_init(NULL, saadc_callback);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_drv_saadc_channel_init(0, &channel_config);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_drv_saadc_buffer_convert(m_buffer_pool[0], SAMPLES_IN_BUFFER);
    APP_ERROR_CHECK(err_code);

//    err_code = nrf_drv_saadc_buffer_convert(m_buffer_pool[1], SAMPLES_IN_BUFFER);
//    APP_ERROR_CHECK(err_code);

}

/**@brief Function for handling the idle state (main loop). If there is no pending log operation,
          then sleeps until the next event occurs.
 */
static void idle_state_handle(void)
{
    if (NRF_LOG_PROCESS() == false)
    {
        nrf_pwr_mgmt_run();
    }
}
/**@brief Function for initializing the application main entry.
 */
int main(void)
{
    sys_init();
    int counter = 0;

#if ON_INDICATE
    ws2812_init();
    saadc_init();
    mpu6050_init();
    MFRC522_Init();
    cmd_mgmt_init();


    buzzer_init();
    motor_init();  
    buzzer_run(NOTE_C,100);
    motot_run(100);
#endif
/*********for test CMD process*********/
//    char buffer_recive[16]= "(100)";
//    cmd_mgmt_take_tag(buffer_recive);
//    strcpy(buffer_recive,"(210,90)");
//    cmd_mgmt_take_tag(buffer_recive);
//
//    strcpy(buffer_recive,"(1001)");
//    cmd_mgmt_take_tag(buffer_recive);
//    
//    if(is_cmpl_program_cmd()){
//        uint8_t cmd_arr_size = (uint8_t) cmd_mgmt_get_size_cmd();
//        char text[cmd_arr_size*24];
//        get_cmpl_program_cmd(&text[0]);
//        NRF_LOG_INFO("cmd_program:%d-%d\n",cmd_arr_size,strlen(&text[0]));
//        reset_cmd_arr(false);
//    } else {
//     char* cmd = cmd_mgmt_get_last_cmd_2();
//     NRF_LOG_INFO("save:%s\n",cmd);
//    }
//        for (;;)
//    {};
/*********************************/
    // Start execution.
    advertising_start(false);
    printf("advertising_start");
/***********/
    // Enter main loop.
    for (;;)
    {
        NRF_LOG_FLUSH();
//        NRF_LOG_PROCESS();
        idle_state_handle();
//>>>>>>>>>>START for MPU******************/
////////        if(NRF_LOG_PROCESS() == false)
////////        {
////////            // Read accelerometer sensor values
////////            accel_values_t acc_values;
////////            ret_code_t err_code = app_mpu_read_accel(&acc_values);
////////            APP_ERROR_CHECK(err_code);
////////            // Clear terminal and print values
////////            NRF_LOG_INFO("%06d %06d %06d", acc_values.x, acc_values.y, acc_values.z);
////////            
////////        }
        if(is_IRQ_mpu) {
            counter++;
            buzzer_run(NOTE_C,100);
            motot_run(100);
            NRF_LOG_INFO("%d",counter);
            /**********************START for BLE******************/
           if(monte_check_connection()){
                char * text_send;// = "123456789012345678901234567890";
                   if(is_cmpl_program_cmd()){
                      uint8_t cmd_arr_size = (uint8_t) cmd_mgmt_get_size_cmd();
                      char text[cmd_arr_size*24];
                      memset(&text[0],0,24);
                      get_cmpl_program_cmd(&text[0]);
                      text_send=text;
                      NRF_LOG_INFO("size:%d len%d\n",cmd_arr_size,strlen(&text[0]));
                      NRF_LOG_INFO("cmd_prog:%s\n",text_send);
                      nrf_delay_us( 10000 );
                      monte_cmd_send(text, strlen(&text[0])); //send cmd
                      reset_cmd_arr(false);
                      cmd_evt_t evt =CMD_SEND_COMPLETED;
                      ws_device_show(PROGRAM_STT,&evt);
                  } else {
                    char text[32];// = cmd_mgmt_get_last_cmd_2();
                    memset(text,0,32);
                    strcpy(text,cmd_mgmt_get_last_cmd_2());
                    pack_cmd_to_program(text);
                    NRF_LOG_INFO("last cmd:%s\n",text);
                    monte_cmd_send(text, strlen(&text[0]));
                  }
//                  NRF_LOG_INFO("Send...:%s-%d",text_send,strlen(text_send));
                  NRF_LOG_FLUSH();
                  nrf_delay_ms(100);
        }
        is_IRQ_mpu=false;
        }
/************************END for BLE******************/

//<<<<<<<<<<<<<<<<<<<END for MPU******************/

/**********************START for MFRC522******************/
      if(tag_read_process_allow) {
      tag_read_process();
      tag_read_process_allow = false;
      }
/************************END for MFRC522******************/


    }
}
