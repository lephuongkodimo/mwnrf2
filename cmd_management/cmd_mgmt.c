#include "cmd_mgmt.h"
#define debug_printf(...) printf(__VA_ARGS__); printf("\n");
#define debug_printf_u(...) NRF_LOG_INFO(__VA_ARGS__)
static cmd_evt_handler_t m_cmd_evt_handler_t = NULL;

/*tao buffer roi truyen vaoo _data*/
monte_err readblock(char* str,char _data[]){
		char *position_ptr = strchr(str,'(');
		char *r_position_ptr = strrchr(str, ')');
		int position = (position_ptr == NULL ? -1 : position_ptr - str);
		int r_position = (r_position_ptr == NULL ? -1 : r_position_ptr - str);
		int len = r_position-position-1;
//                debug_printf("position:%d__position_r:%d__len:%d",position,r_position,len);
//                NRF_LOG_INFO("position:%d position_r:%d len:%d",position,r_position,len);
		if(position<0 || r_position<0 || len <= 0) {NRF_LOG_INFO("NULLL");return -1;}
                if (!_data) {
                    NRF_LOG_INFO("NNNULLL");
                    return -1;
                }
                _data[len]='\0';
                memcpy(_data,position_ptr+1,len);
                return 0;
}
monte_err split_string(char* str,int *n,char splits[][8]) {
//NRF_LOG_INFO("str:%s",(uint32_t)str);
//    NRF_LOG_HEXDUMP_INFO(str,32);
//    char** splits;
    char* token = strtok(str, ",");
//    NRF_LOG_INFO("1111:%d",spaces);
//    NRF_LOG_HEXDUMP_INFO(token,8);
    int spaces = 0;
    if(token==NULL) {
      strcpy(splits[0],str);
      *n = 1;
      return 0;
    }
    while (token) {
        ++spaces;
//        splits[4*spaces];
//        splits = nrf_realloc(splits, sizeof(char*) * ++spaces);
        if (!splits) {
            NRF_LOG_INFO("NNNULLLLLLLLLLLLL");
            return -1;
        }

//        splits[spaces - 1] = token;
        memcpy(&splits[spaces - 1],token,8);
        token = strtok(NULL, ",");
//        NRF_LOG_INFO("spaces:%d",spaces);
//        NRF_LOG_HEXDUMP_INFO(splits[spaces-1],8);
//        NRF_LOG_HEXDUMP_INFO(token,8);
    }
    *n = spaces;
    return 0;
}
monte_err split_header_string(char* str,char* header_out,char* data_out) {
         char *position_ptr = strchr(str,',');
          printf("data:%s\n",position_ptr+1);
          data_out =position_ptr+1;
          char* token = strtok(str, ",");
          header_out=token;
}


void cmd_mgmt_set_handler(cmd_evt_handler_t evt_handler){      
    m_cmd_evt_handler_t = evt_handler;
}

static int cmd_arr_count = 0; // dem so cmd
static char cmd_arr_store[50][4][8];
static uint8_t cmd_one_index_count[50]; // dem so index trong moi cmd
static char cmd_last[4][8];
static bool m_is_present_begin_cmd  =false;
static bool m_is_present_end_cmd =false;
static bool m_is_cmpl_program_cmd = false;
static cmd_evt_t last_evt_cmd;
static vector v_cmd_store;
//[123][123] -> n=2
void cmd_init(cmd_evt_handler_t evt_handler){
  vector_init(&v_cmd_store);
  cmd_mgmt_set_handler(evt_handler);
}
void cmd_mgmt_add(char arr_temp[][8],int n){ 
NRF_LOG_INFO(">>>ADD")
    for(int j = 0; j < n ;j++){
      memcpy(&cmd_arr_store[cmd_arr_count][j],&arr_temp[j],8);
      }
      cmd_one_index_count[cmd_arr_count]=n;
    for (int i = 0; i < n; i++) {
//      NRF_LOG_HEXDUMP_INFO(cmd_arr_store[cmd_arr_count][i],8);
      NRF_LOG_FLUSH();
    }
    cmd_arr_count++;
NRF_LOG_INFO("<<<ADD")
}
void cmd_printf(){
  for(int k = 0; k<cmd_arr_count;k++){
////NRF_LOG_INFO("cmd_arr_store[%d]:",k);
      NRF_LOG_INFO(">>cmd_printf:%d\n",k);
        uint8_t j= cmd_one_index_count[k];
        for (uint8_t i = 0; i < j; i++) {
                size_t l = strlen(cmd_arr_store[k][i]);
          NRF_LOG_HEXDUMP_INFO(cmd_arr_store[k][i],l);
          printf("[%s]",cmd_arr_store[k][i]);
          NRF_LOG_FLUSH();
        }
        NRF_LOG_INFO("<<cmd_printf\n");
      }
}
char   cmd_save[32];
cmd_t  m_cmd_store[50];
cmd_t* get_cmd_store() {return &m_cmd_store[0];}
int c_cmd =0;
void cmd_mgmt_take_tag(uint8_t* buffer_recive){
//      NRF_LOG_INFO("sizeof(char*):%d",sizeof(char*));
        NRF_LOG_INFO(">>>TAKE TAG")     
        cmd_evt_t evt; //evt block
        char outdata[32]; //-> 123,123,123
        char arr_temp[4][8]; // [123][123][123]
        if(buffer_recive==0) {                  
          if(last_evt_cmd==CMD_START_BLOCK_PIANO) {
            evt = CMD_FINISH_BLOCK_PIANO;
            }else {
            evt = CMD_FINISH_BLOCK;
            }
        } else {
          memset(outdata,0,32);
          for(int z= 0;z <4;z++){
            memset(&arr_temp[z],0,8);
          }
          int n;
          monte_err err = readblock((char*)buffer_recive,outdata);
          if(err!=0) {NRF_LOG_INFO("not cmd"); return;}
          strcpy(cmd_save, outdata);
          strcat(cmd_save,";");
          split_string(outdata,&n,&arr_temp[0]);

          //save last cmd



        
  //        //slip cmd*****
  //        char *position_ptr = strchr(outdata,',');
  //        printf("header:%s ",position_ptr);
  //        char data_out[strlen(position_ptr+1)];// =position_ptr+1;
  //        strcpy(data_out,position_ptr+1);
  //        char* token = strtok(outdata, ",");
  //        char header_out[strlen(token)];// = token;
  //        strcpy(header_out,token);
  //        printf("header:%s -- data:%s -- n:%d -- %d --- %d\n",token,(char*)(position_ptr),n,strlen(position_ptr+1),strlen(token));
  //        //************
  //        ;
  //        cmd_t _cmd = {
  //                .cmd_header = &buff[0],
  //                .cmd_data = data_out,
  //                .num_count = n,
  //        };
  //        m_cmd_store[cmd_arr_count] = _cmd;
  //        cmd_arr_count++;
  //        NRF_LOG_INFO("n=:%d",n);

  //        NRF_LOG_FLUSH();

              
  //        for (int i = 0; i < n; i++) { 
  //        NRF_LOG_HEXDUMP_INFO(arr_temp[i],8);
  //        }

          int block_id = atoi( arr_temp[0]);
          
          NRF_LOG_INFO("cmd_header=:%d",block_id);
          cmd_mgmt_add(arr_temp,n);
        
        
          if(m_is_cmpl_program_cmd){
            m_is_present_begin_cmd = false;
            m_is_present_end_cmd = false;
            m_is_cmpl_program_cmd = false; // lay bat ki cmd nao thi program cung mat, program =true duoc kiem tra ben duoi
          }
          if(block_id==CMD_BLOCK_ID_BEGIN) {
            m_is_present_begin_cmd = true;
            m_is_present_end_cmd = false;
            reset_cmd_arr(false);
            cmd_mgmt_add(arr_temp,n); // clear het nen add 1 cai begin lai
           }
          if(block_id==CMD_BLOCK_ID_END || block_id==CMD_BLOCK_ID_END_LOOP) m_is_present_end_cmd = true;
          if(m_is_present_begin_cmd && m_is_present_end_cmd) {
            m_is_cmpl_program_cmd =true;

            }



  //NRF_LOG_INFO("______");
  //        readblock(".(342,2,123)..",outdata);
  //        split_string(outdata,&n,&arr_temp[0]);  
  //        cmd_mgmt_add(arr_temp,n);
  //NRF_LOG_INFO("=====");
  //        readblock(".(11,222,33333)..",outdata);
  //        split_string(outdata,&n,&arr_temp[0]);  
  //        cmd_mgmt_add(arr_temp,n);

  //        cmd_printf();
  //        readblock(".(111,222,333)..",outdata);
  //        split_string(outdata,&n,&arr_temp[0]);
  //          cmd_id_to_cmd_evt(arr_temp);
          
            if(m_is_cmpl_program_cmd){
               evt = CMD_ON_COMPLETED_PROGRAM;
            } else {
               evt = cmd_id_to_cmd_evt(arr_temp);

            }
            last_evt_cmd = evt;
          }//end finsih block
          if(evt==CMD_START_BLOCK_PIANO){
            int tone_id = atoi( arr_temp[1]);
            NRF_LOG_INFO("tone_id=:%d",tone_id);
            note_t _tone = tone_id_to_tone_t(tone_id);
            m_cmd_evt_handler_t(&evt,&_tone);
          } else {           
            m_cmd_evt_handler_t(&evt,NULL);
          }
          NRF_LOG_INFO("cmd_arr_count:%d",cmd_arr_count)
          NRF_LOG_INFO("<<<TAKE TAG")
}
note_t tone_id_to_tone_t(int id){
		switch(id){
		case 1:
			return NOTE_C;
		case 2:
			return NOTE_D;
		case 3:
			return NOTE_E;
		case 4:{
			return NOTE_F;
		}
		case 5:{
			return NOTE_G;
		}
		case 6:{
			return NOTE_A;
		}
		case 7:{
			return NOTE_B;
		}case 8:{
			return NOTE_MAX;
		}
		default :
			return NOTE_MAX;
		}
}
int cmd_id_to_cmd_evt(char _cmd[4][8]){
    int block_id = atoi(_cmd[0]);
    switch(block_id){
     case CMD_BLOCK_ID_BEGIN:
      return CMD_ON_BLOCK_BEGIN;
     case CMD_BLOCK_ID_END:
      return CMD_ON_BLOCK_END;
     case CMD_BLOCK_ID_END_LOOP:
      return CMD_ON_BLOCK_END_LOOP;
     case CMD_BLOCK_ID_TURN:{
      int id = atoi(_cmd[1]);
      if(id<0) {return CMD_ON_BLOCK_TURNRIGHT;}
      else {return CMD_ON_BLOCK_TURNLEFT;}   
        }
     case CMD_BLOCK_ID_MOVE:{
     int id = atoi(_cmd[1]);
      if(id>0) {return CMD_ON_BLOCK_MFORWARD;} else 
                {return CMD_ON_BLOCK_MBACKWARD;}
                }
     case CMD_BLOCK_ID_COUND:
     case CMD_BLOCK_ID_PIANO:
      return CMD_START_BLOCK_PIANO;
     default:
     return CMD_ON_BLOCK_UNKNOW;
      break;
    }

}
void cmd_mgmt_get_last_cmd(char arr_temp[][8],int* n){
        *n=0;
        for(int z= 0;z <4;z++){
          size_t len = strlen(cmd_last[z]);
          if(len!=0) *n= (*n)+1;
//          memcpy(&arr_temp[z],&cmd_last[z],len);
          strcpy((char *)arr_temp[z],(char *)cmd_last[z]);
        }
              
}
void cmd_mgmt_get_last_cmd_pionter(char* arr_temp[],int* n){
        *n=0;
        for(int z= 0;z <4;z++){
          if(strlen(cmd_last[z])!=0) *n= (*n)+1;
          strcpy((char *)arr_temp[z],(char *)cmd_last[z]);
//          memcpy(&arr_temp[z],&cmd_last[z],8);
        }
              
}
char* cmd_mgmt_get_last_cmd_2(){
  return cmd_save;
}
bool is_cmpl_program_cmd(){
  return m_is_cmpl_program_cmd;
}
static const char *dau_cau[] = {",",";","(",")" };
vector* get_cmpl_program_cmd_v(){
    return &v_cmd_store;
}
void pack_cmd_to_program(char* text){
debug_printf_u(">>cmd to pro")
char mtext[32];
char * begin = "100;";
char * end = "1000;";
strcpy(mtext,begin);
strcat(mtext,text);
strcat(mtext,end);
strcpy(text,mtext);
debug_printf_u("cmd:%s :%s",text,mtext);
debug_printf_u("<<cmd to pro")
}
void get_cmpl_program_cmd(char* program_out){
    NRF_LOG_INFO(">>GET PROG");
//    char cmd_text[cmd_arr_count*24];
        NRF_LOG_INFO("cmd_arr_count:%d",cmd_arr_count);
    for(int i=0;i<cmd_arr_count;i++){
    uint8_t dau_cau_id = 0;
    //dem so index trong moi cmd
          uint8_t c_index= cmd_one_index_count[i];
//             for(int j=0;j<4;j++){
////                debug_printf_u("cmd00:'%s'",cmd_arr_store[0][0]);
//                 NRF_LOG_HEXDUMP_INFO(cmd_arr_store[0][0],8);
//              if(strlen(cmd_arr_store[i][j])==0) break;
//              c_index++; 
//             }
              
             debug_printf_u("cmd_count_index=%d",c_index);
             for(int j=0;j<c_index;j++){
              if(j==(c_index-1)) dau_cau_id = 1; // cai cuoi cung
                      debug_printf_u("__>__")
                      strcat(program_out, cmd_arr_store[i][j]);
                      NRF_LOG_HEXDUMP_INFO(cmd_arr_store[i][j],16);
                      NRF_LOG_HEXDUMP_INFO(program_out,16);
                      debug_printf_u("__<__");
                      strcat(program_out, dau_cau[dau_cau_id]);
                      debug_printf_u("cmd[%d][%d]:'%s'",i,j,program_out);
                      NRF_LOG_HEXDUMP_INFO(program_out,16);
              }

          debug_printf_u("to next cmd");
    }

    size_t l = strlen(program_out);

//    NRF_LOG_INFO(">>");
    NRF_LOG_FLUSH();
//    SEGGER_RTT_printf(0,"Count %d:'%s'\n",l,program_out);
    NRF_LOG_INFO("Count %d:'%s'\n",l,program_out);
    nrf_delay_us( 10000 );   // Delays roughly 10 msec
//    NRF_LOG_HEXDUMP_INFO(cmd_text,l);
//    NRF_LOG_INFO("<<");
//    memcpy(&program_out[0],&cmd_text[0],l);
    NRF_LOG_INFO("<<GET PROG");
}
//tinh so luong index cua 1 cmd
int cmd_mgmt_calcula_size_one_cmd(){
}
void reset_cmd_arr(bool from_begin){
      for(int i=0;i<cmd_arr_count;i++){
      for(int j=0;j<4;j++){
          memset(cmd_arr_store[i][j],0,8);
        }
}
memset(cmd_one_index_count,0,50); // clear dem so index trong moi cmd
      cmd_arr_count=0; 
      m_is_cmpl_program_cmd = false; 
      ////from_begin==1?(cmd_arr_count = 1):(cmd_arr_count=0);
      }
      //so cmd da luu
      int cmd_mgmt_get_size_cmd(){
        return cmd_arr_count;
}