#include "motor.h"
static nrf_mtx_t            m_motor_mtx;       /**< Module API lock.*/
static volatile bool flag_done;
APP_TIMER_DEF(m_motor_single_shot_timer);  /**< Handler for single shot timer used to light LED 2. */
static void single_shot_motor_timer_handler(void * p_context)
{
  nrf_gpio_pin_clear(MOTOR_PIN);
  flag_done = true;
}
/*TEST PWM*/
void motor_init(){
            nrf_gpio_cfg_output(MOTOR_PIN);
            nrf_gpio_pin_clear(MOTOR_PIN);
            ret_code_t err_code = app_timer_create(&m_motor_single_shot_timer,
                                        APP_TIMER_MODE_SINGLE_SHOT,
                                        single_shot_motor_timer_handler);
            APP_ERROR_CHECK(err_code);
            nrf_mtx_init(&m_motor_mtx);
            flag_done  =true;
}
void motot_run(uint32_t timeout){
            if (!nrf_mtx_trylock(&m_motor_mtx) || !flag_done)
            {
                return;
            }
            nrf_gpio_pin_set(MOTOR_PIN);
            ret_code_t err_code = app_timer_start(m_motor_single_shot_timer, APP_TIMER_TICKS(timeout), NULL);
            APP_ERROR_CHECK(err_code);
            nrf_mtx_unlock(&m_motor_mtx);
}