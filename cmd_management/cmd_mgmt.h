#ifndef MT_CMD_MGMT_H
#define MT_CMD_MGMT_H 


#include "nrf_log.h"
#include <string.h>
#include "monte_erro.h"
#include "my_vector.h"
#include "mem_manager.h"
#include "nrf_log_ctrl.h"
#include "SEGGER_RTT.h"
#include "nrf_delay.h"
#include "buzzer.h"
#include <stdlib.h>
typedef struct cmd_t {
  char* cmd_header;//210
  char* cmd_data;//90
  uint8_t num_count; //=2 , includeheader + data
} cmd_t;
typedef struct myvector {
    int *items;
//    int capacity;
//    int total;
} myvector;

typedef enum{
     CMD_ON_BLOCK_BEGIN = 0,
     CMD_ON_BLOCK_END,
     CMD_ON_BLOCK_END_LOOP,
     CMD_ON_BLOCK_MFORWARD,
     CMD_ON_BLOCK_MBACKWARD,
     CMD_ON_BLOCK_TURNLEFT,
     CMD_ON_BLOCK_TURNRIGHT,
     CMD_ON_COMPLETED_PROGRAM,
     CMD_START_BLOCK_PIANO,
     CMD_FINISH_BLOCK_PIANO,
     CMD_FINISH_BLOCK,
     CMD_SEND_COMPLETED,
     CMD_ON_BLOCK_UNKNOW
}cmd_evt_t; 
typedef enum{
      CMD_BLOCK_ID_BEGIN = 100,
     CMD_BLOCK_ID_END =1000,
     CMD_BLOCK_ID_END_LOOP =1001,
     CMD_BLOCK_ID_TURN = 210,
     CMD_BLOCK_ID_MOVE = 200,
     CMD_BLOCK_ID_COUND = 300,
     CMD_BLOCK_ID_PIANO = 301,
     CMD_BLOCK_ID_MFORWARD = -15,
     CMD_BLOCK_ID_MBACKWARD = 15,
     CMD_BLOCK_ID_TURNLEFT = 90,
     CMD_BLOCK_ID_TURNRIGHT = -90,

}cmd_block_t;
typedef void (*cmd_evt_handler_t) (cmd_evt_t const * p_cmd_evt, void * p_context);
void cmd_mgmt_set_handler(cmd_evt_handler_t evt_handler);
void cmd_mgmt_take_tag(uint8_t* buffer_recive);
void cmd_printf();

void cmd_mgmt_get_last_cmd();
bool is_cmpl_program_cmd();
void get_cmpl_program_cmd();
void reset_cmd_arr(bool from_begin);
int cmd_mgmt_get_size_cmd();
int cmd_id_to_cmd_evt(char _cmd[4][8]);
note_t tone_id_to_tone_t(int id);

vector * get_cmpl_program_cmd_v();
void cmd_init(cmd_evt_handler_t evt_handler);
char* get_cmd_save();
char** get_cmd_save2d();
cmd_t* get_cmd_store();
char* cmd_mgmt_get_last_cmd_2();
void pack_cmd_to_program(char* text);

#endif