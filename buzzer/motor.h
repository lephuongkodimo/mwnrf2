#ifndef MT_MOTOR_H
#define MT_MOTOR_H 
#include "app_timer.h"
#include "app_pwm.h"
#include "nrf_mtx.h"
#include "nrf_gpio.h"
#include "nrf_log.h"
#define   MOTOR_PIN 19
void motor_init();
void motot_run(uint32_t timeout);
#endif