#ifndef SYSTEM_H_
#define SYSTEM_H_
#include <stdbool.h>
void sys_init(void);
void advertising_start(bool erase_bonds);
#endif