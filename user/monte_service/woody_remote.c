
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "nordic_common.h"
#include "app_error.h"
#include "ble_db_discovery.h"
#include "app_timer.h"
#include "app_util.h"
#include "bsp_btn_ble.h"
#include "ble.h"
#include "ble_gap.h"
#include "ble_hci.h"
#include "nrf_sdh.h"
#include "nrf_sdh_ble.h"
#include "nrf_sdh_soc.h"
#include "montes_c.h"
#include "nrf_ble_gatt.h"
#include "nrf_pwr_mgmt.h"
#include "nrf_ble_scan.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "woody_remote.h"

BLE_MONTE_BLOCK_C_DEF(m_ble_monte_blocks_c);    
static monte_blocks_c_handler_t monte_blocks_c_handler=NULL;
static bool isWoodyConnected =false;
////sau dung cai bien kiem tra so kte noi
//static void set_monte_blocks_c_handler(monte_blocks_c_handler_t* monte_blocks_c_evt_handler){
//    monte_blocks_c_handler = monte_blocks_c_evt_handler; 
//}
static void ble_monte_blocks_c_evt_handler(ble_monte_blocks_c_t * p_ble_monte_blocks_c, ble_monte_blocks_c_evt_t const * p_ble_monte_blocks_evt)
{
    ret_code_t err_code;
    printf("ble_monte_blocks_c_evt_handler\n");
    if(p_ble_monte_blocks_evt->evt_type == BLE_MONTE_BLOCKS_C_EVT_DISCOVERY_COMPLETE) {
        NRF_LOG_INFO("Discovery complete.");
        printf("Discovery complete.\n");
        err_code = ble_monte_blocks_c_handles_assign(p_ble_monte_blocks_c, p_ble_monte_blocks_evt->conn_handle, &p_ble_monte_blocks_evt->handles);
        APP_ERROR_CHECK(err_code);
//            err_code = ble_nus_c_tx_notif_enable(p_ble_nus_c);
//            APP_ERROR_CHECK(err_code);
      isWoodyConnected = true;
        NRF_LOG_INFO("Connected to device with Monte Service.");
    } else if(p_ble_monte_blocks_evt->evt_type == BLE_MONTE_BLOCKS_C_EVT_DISCONNECTED){
      isWoodyConnected = false;
    }

    if(monte_blocks_c_handler!=NULL) monte_blocks_c_handler(p_ble_monte_blocks_evt);
}
/**@brief Function for initializing the Nordic UART Service (NUS) client. */
void monte_blocks_c_init(monte_blocks_c_init_t* monte_blocks_c_init)
{
    ret_code_t       err_code;
    ble_monte_blocks_c_init_t init;

    init.evt_handler   = ble_monte_blocks_c_evt_handler;
    init.error_handler = monte_blocks_c_init->error_handler;
    init.p_gatt_queue  = monte_blocks_c_init->p_gatt_queue;

    err_code = ble_monte_blocks_c_init(&m_ble_monte_blocks_c, &init);
    APP_ERROR_CHECK(err_code);
    monte_blocks_c_handler=monte_blocks_c_init->evt_handler;
    
}
//send cmd
void monte_cmd_send(char* text,uint16_t len)
{
    ble_monte_blocks_c_string_send(&m_ble_monte_blocks_c, text, len);
}
//khoi tao discovery db 
void monte_disc_evt(ble_db_discovery_evt_t * p_evt){
    ble_monte_blocks_c_on_db_disc_evt(&m_ble_monte_blocks_c,p_evt);
}
//check Is device specfic connected?
bool monte_check_connection(){
    return isWoodyConnected;
    }