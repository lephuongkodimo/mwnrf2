

#include <stdint.h>
#include <stdbool.h>
#include "ble.h"
#include "ble_gatt.h"
#include "ble_db_discovery.h"
#include "ble_srv_common.h"
#include "nrf_ble_gq.h"
#include "nrf_sdh_ble.h"

#include "sdk_config.h"
/**@brief   Macro for defining a ble_nus_c instance.
 *
 * @param   _name   Name of the instance.
 * @hideinitializer
 */
#define BLE_MONTE_BLOCK_C_DEF(_name)                                                                        \
static ble_monte_blocks_c_t _name;                                                                           \
NRF_SDH_BLE_OBSERVER(_name ## _obs,                                                                 \
                     BLE_NUS_C_BLE_OBSERVER_PRIO,                                                   \
                     ble_monte_blocks_c_on_ble_evt, &_name)

                    
#define MONTE_BASE_UUID                   {{0x92, 0x77, 0x3e, 0x1e, 0x82, 0xE6, 0x48, 0x85, 0x0e, 0x49, 0xea, 0xf5, 0x00, 0x00, 0x61, 0x62}} /**< Used vendor-specific UUID. */
#define BLE_UUID_ADV            0x0000  
#define BLE_UUID_BLOCKS_SERVICE            0xCB01                      /**< The UUID of the Nordic UART Service. */
#define BLE_UUID_BLOCKS_PROGRAM_CHARACTERISTIC  0xEC06                      /**< The UUID of the RX Characteristic. */
#define BLE_UUID_BLOCKS_BLOCK_CHARACTERISTIC  0x4C14                      /**< The UUID of the TX Characteristic. */

#define BLE_MONTE_BLOCKS_MAX_DATA_LEN (NRF_SDH_BLE_GATT_MAX_MTU_SIZE - 2 - 1)
/**@brief Block Client event type. */
typedef enum
{
    BLE_MONTE_BLOCKS_C_EVT_DISCOVERY_COMPLETE,   /**< Event indicating that the NUS service and its characteristics were found. */
    BLE_MONTE_BLOCKS_C_EVT_PROGRAM_EVT,           /**< Event indicating that the central received something from a peer. */
    BLE_MONTE_BLOCKS_C_EVT_DISCONNECTED          /**< Event indicating that the NUS server disconnected. */
} ble_monte_blocks_c_evt_type_t;

/**@brief Handles on the connected peer device needed to interact with it. */
typedef struct
{
    uint16_t blocks_program_handle;      /**< Handle of the NUS TX characteristic, as provided by a discovery. */
    uint16_t blocks_program_cccd_handle; /**< Handle of the CCCD of the NUS TX characteristic, as provided by a discovery. */
    uint16_t blocks_block_handle;      /**< Handle of the NUS RX characteristic, as provided by a discovery. */
} ble_monte_blocks_c_handles_t;
/**@brief Structure containing the NUS event data received from the peer. */
typedef struct
{
    ble_monte_blocks_c_evt_type_t evt_type;
    uint16_t             conn_handle;
    uint16_t             max_data_len;
    uint8_t            * p_data;
    uint16_t             data_len;
    ble_monte_blocks_c_handles_t  handles;     /**< Handles on which the Nordic UART service characteristics were discovered on the peer device. This is filled if the evt_type is @ref BLE_NUS_C_EVT_DISCOVERY_COMPLETE.*/
} ble_monte_blocks_c_evt_t;

// Forward declaration of the ble_nus_t type.
typedef struct ble_monte_blocks_c_s ble_monte_blocks_c_t;
/**@brief   Event handler type.
 *
 * @details This is the type of the event handler that is to be provided by the application
 *          of this module to receive events.
 */
typedef void (* ble_monte_blocks_c_evt_handler_t)(ble_monte_blocks_c_t * p_ble_monte_blocks_c, ble_monte_blocks_c_evt_t const * p_evt);
/**@brief NUS Client structure. */
struct ble_monte_blocks_c_s
{
    uint8_t                   uuid_type;      /**< UUID type. */
    uint16_t                  conn_handle;    /**< Handle of the current connection. Set with @ref ble_nus_c_handles_assign when connected. */
    ble_monte_blocks_c_handles_t       handles;        /**< Handles on the connected peer device needed to interact with it. */
    ble_monte_blocks_c_evt_handler_t   evt_handler;    /**< Application event handler to be called when there is an event related to the NUS. */
    ble_srv_error_handler_t   error_handler;  /**< Function to be called in case of an error. */
    nrf_ble_gq_t            * p_gatt_queue;   /**< Pointer to BLE GATT Queue instance. */
};


/**@brief NUS Client initialization structure. */
typedef struct
{
    ble_monte_blocks_c_evt_handler_t   evt_handler;    /**< Application event handler to be called when there is an event related to the NUS. */
    ble_srv_error_handler_t   error_handler;  /**< Function to be called in case of an error. */
    nrf_ble_gq_t            * p_gatt_queue;   /**< Pointer to BLE GATT Queue instance. */
} ble_monte_blocks_c_init_t;

uint32_t ble_monte_blocks_c_init(ble_monte_blocks_c_t * p_ble_monte_blocks_c, ble_monte_blocks_c_init_t * p_ble_monte_blocks_c_init);
 void ble_monte_blocks_c_on_db_disc_evt(ble_monte_blocks_c_t * p_ble_monte_blocks_c, ble_db_discovery_evt_t * p_evt);
 void ble_monte_blocks_c_on_ble_evt(ble_evt_t const * p_ble_evt, void * p_context);
 uint32_t ble_monte_blocks_c_tx_notif_enable(ble_monte_blocks_c_t * p_ble_monte_blocks_c);
 uint32_t ble_monte_blocks_c_handles_assign(ble_monte_blocks_c_t *               p_ble_monte_blocks_c,
                                  uint16_t                    conn_handle,
                                  ble_monte_blocks_c_handles_t const * p_peer_handles);
uint32_t ble_monte_blocks_c_string_send(ble_monte_blocks_c_t * p_ble_monte_blocks_c, uint8_t * p_string, uint16_t length);
