#ifndef MT_BUZZER_H
#define MT_BUZZER_H 
#include "app_pwm.h"
#include "nrf_mtx.h"
#include "app_timer.h"
#include "nrf_gpio.h"
#include "nrf_log.h"
#define  BUZZER_PIN 12
#define  TIMEOUT_BUZZER_PIANO 2000//ms
typedef enum {
    NOTE_C, NOTE_Cs, NOTE_D, NOTE_Eb, NOTE_E, NOTE_F, NOTE_Fs, NOTE_G, NOTE_Gs, NOTE_A, NOTE_Bb, NOTE_B, NOTE_MAX
} note_t;
enum {
BUZZER_INIT_PWM_DEFAULT,
BUZZER_INIT_PWM_PIANO,
BUZZER_DEINIT_PWM_DEFAULT,
BUZZER_DEINIT_PWM_PIANO
};
typedef struct buzzer_t{
  note_t notes[5];
  uint32_t timeout_per_note;
  uint8_t timerepeat;
}buzzer_t;
void buzzer_run(note_t note,uint32_t timeout);
uint32_t tone_to_freq(note_t note, uint8_t octave);
void buzzer_init();
void buzzer_run(note_t note,uint32_t timeout);
void buzzer_run_array(buzzer_t* tones,uint32_t break_time);
void buzzer_piano_start(note_t note);
void buzzer_piano_stop();

#endif
