#include "buzzer.h"

APP_TIMER_DEF(m_single_shot_timer_id);  /**< Handler for single shot timer used to light LED 2. */
APP_TIMER_DEF(m_repeat_timer_control);  /**< Handler for single shot timer used to light LED 2. */

buzzer_t tone_connected = {
    .notes = {NOTE_C,NOTE_C},
    .timeout_per_note = 100,
    .timerepeat = 2
};
static nrf_mtx_t            m_buzzer_mtx;       /**< Module API lock.*/
static volatile bool flag_done;
static int pwm_index = BUZZER_DEINIT_PWM_DEFAULT;
static uint8_t counter_note =0;
APP_PWM_INSTANCE(PWM1,1); 

static volatile bool ready_flag;            // A flag indicating PWM status.
static void buzzer_stop_done_flag();
static void repeat_timer_handler(void * p_context)
{
  buzzer_t * buz = (buzzer_t *) p_context;
  if(buz->timerepeat == counter_note){
    counter_note=0;
    app_timer_stop(m_repeat_timer_control);}
    else{
    buzzer_run(buz->notes[counter_note],buz->timeout_per_note);
    counter_note++;
  }
}
static void single_shot_timer_handler(void * p_context)
{
  buzzer_stop_done_flag(); //stop pwm adn done flag 
}


static void pwm_ready_callback(uint32_t pwm_id)    // PWM callback function
{
    ready_flag = true;
}

uint32_t tone_to_freq(note_t note, uint8_t octave){
    const uint16_t noteFrequencyBase[12] = {
    //   C        C#       D        Eb       E        F       F#        G       G#        A       Bb        B
        4186,    4435,    4699,    4978,    5274,    5588,    5920,    6272,    6645,    7040,    7459,    7902
    };

    if(octave > 8 || note >= NOTE_MAX){
        return 0;
    }
    uint32_t noteFreq =  (uint32_t)noteFrequencyBase[note] / (double)(1 << (8-octave));
    return noteFreq;
}
/*TEST PWM*/
void buzzer_init(){
            ret_code_t err_code = app_timer_create(&m_single_shot_timer_id,
                                        APP_TIMER_MODE_SINGLE_SHOT,
                                        single_shot_timer_handler);
            APP_ERROR_CHECK(err_code);
            err_code = app_timer_create(&m_repeat_timer_control,
                                        APP_TIMER_MODE_REPEATED,
                                        repeat_timer_handler);
            APP_ERROR_CHECK(err_code);
            uint32_t m_tone_freq = tone_to_freq(NOTE_C,4);
            app_pwm_config_t pwm1_cfg = APP_PWM_DEFAULT_CONFIG_1CH(m_tone_freq,BUZZER_PIN);
            /* Switch the polarity of the second channel. */
            pwm1_cfg.pin_polarity[1] = APP_PWM_POLARITY_ACTIVE_HIGH;

            /* Initialize and enable PWM. */
            pwm_index=BUZZER_INIT_PWM_DEFAULT;
            err_code = app_pwm_init(&PWM1,&pwm1_cfg,NULL);
            APP_ERROR_CHECK(err_code);
            nrf_mtx_init(&m_buzzer_mtx);
            flag_done  =true;
            
}
//call from interup
void buzzer_run(note_t note,uint32_t timeout){
            if (!nrf_mtx_trylock(&m_buzzer_mtx) || !flag_done)
            {
                return;
            }
            ret_code_t err_code;
            if(pwm_index==BUZZER_INIT_PWM_PIANO || pwm_index==BUZZER_DEINIT_PWM_DEFAULT) {
              app_pwm_uninit(&PWM1);


              uint32_t m_tone_freq = tone_to_freq(NOTE_C,4);
              app_pwm_config_t pwm1_cfg = APP_PWM_DEFAULT_CONFIG_1CH(m_tone_freq,BUZZER_PIN);
              /* Switch the polarity of the second channel. */
              pwm1_cfg.pin_polarity[1] = APP_PWM_POLARITY_ACTIVE_HIGH;

              /* Initialize and enable PWM. */
              pwm_index=BUZZER_INIT_PWM_DEFAULT;
              err_code = app_pwm_init(&PWM1,&pwm1_cfg,NULL);
              APP_ERROR_CHECK(err_code);
            } else
            if(pwm_index==BUZZER_INIT_PWM_DEFAULT){

            } 
              uint32_t value=50; //(i < 20) ? (i * 5) : (100 - (i - 20) * 5);
              /* Set the duty cycle - keep trying until PWM is ready... */
              app_pwm_enable(&PWM1);
              while (app_pwm_channel_duty_set(&PWM1, 0, value) == NRF_ERROR_BUSY);
              err_code = app_timer_start(m_single_shot_timer_id, APP_TIMER_TICKS(timeout), NULL);
              APP_ERROR_CHECK(err_code);
            nrf_mtx_unlock(&m_buzzer_mtx);
}
//not reocmment call from interup!!!!!!!
void buzzer_run_array(buzzer_t* tones,uint32_t break_time){
            uint32_t timeout = tones->timeout_per_note + break_time;
            ret_code_t err_code = app_timer_start(m_repeat_timer_control, APP_TIMER_TICKS(timeout), tones);
            APP_ERROR_CHECK(err_code);
}
static note_t last_note = NOTE_C;
void buzzer_piano_start(note_t note){
            if (!nrf_mtx_trylock(&m_buzzer_mtx) || !flag_done)
            {
                return;
            }
            /* Initialize and enable PWM. */
            uint32_t err_code;
            if(last_note != note) {
              if(pwm_index==BUZZER_INIT_PWM_DEFAULT || pwm_index==BUZZER_INIT_PWM_PIANO) {
                app_pwm_uninit(&PWM1);
              } 
              uint32_t m_tone_freq = tone_to_freq(note,4);
              app_pwm_config_t pwm1_cfg = APP_PWM_DEFAULT_CONFIG_1CH(m_tone_freq,BUZZER_PIN);
              /* Switch the polarity of the second channel. */
              pwm1_cfg.pin_polarity[1] = APP_PWM_POLARITY_ACTIVE_HIGH;
              err_code = app_pwm_init(&PWM1,&pwm1_cfg,NULL);
              APP_ERROR_CHECK(err_code);
              pwm_index = BUZZER_INIT_PWM_PIANO;
            }  

            //van la note cu 
            app_pwm_enable(&PWM1);
            while (app_pwm_channel_duty_set(&PWM1, 0, 50) == NRF_ERROR_BUSY);
            last_note = note;

            err_code = app_timer_start(m_single_shot_timer_id, APP_TIMER_TICKS(TIMEOUT_BUZZER_PIANO), NULL);
            APP_ERROR_CHECK(err_code);

            nrf_mtx_unlock(&m_buzzer_mtx);
}
void buzzer_piano_stop(){
     buzzer_stop_done_flag();//stop pwm adn done flag 
//  while (app_pwm_channel_duty_set(&PWM1, 0, 0) == NRF_ERROR_BUSY);
//  /**/
//  app_pwm_disable(&PWM1);
//  nrf_gpio_pin_clear(BUZZER_PIN);
//  flag_done  =true;
//  if(is_init_pwm )app_pwm_uninit(&PWM1);
//  uint32_t m_tone_freq = tone_to_freq(NOTE_C,4);
//  app_pwm_config_t pwm1_cfg = APP_PWM_DEFAULT_CONFIG_1CH(m_tone_freq,BUZZER_PIN);
//  /* Switch the polarity of the second channel. */
//  pwm1_cfg.pin_polarity[1] = APP_PWM_POLARITY_ACTIVE_HIGH;
//
//  /* Initialize and enable PWM. */
//  is_init_pwm =  true;

//  uint32_t err_code = app_pwm_init(&PWM1,&pwm1_cfg,NULL);
}
void buzzer_stop_done_flag(){
  while (app_pwm_channel_duty_set(&PWM1, 0, 0) == NRF_ERROR_BUSY);
  /**/
  app_pwm_disable(&PWM1);
  nrf_gpio_pin_clear(BUZZER_PIN);
  flag_done  =true;
}