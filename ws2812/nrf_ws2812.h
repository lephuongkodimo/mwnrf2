#ifndef MT_NRF_WS2812_H
#define MT_NRF_WS2812_H 

#include <stdio.h>
#include "nrf_drv_i2s.h"
#include "nrf_delay.h"
#include "app_error.h"
#include "nrf_log.h"
#include "app_timer.h"
#include "cmd_mgmt.h"
#include "ble.h"
#include "nrf_mtx.h"
#define NRF_WS2812_PIN 17
#define NLEDS 1
#define RESET_BITS 0
typedef enum {
  PRESENT_CMD,
  PROGRAM_STT,
  POWER_STT,
  BLE_DEVICE_STT,
  COLOR_OPTION,
  RESET_LED
}device_evt_t;
//uint32_t ws_color[8] = {
//		0x00ff00, //CMD_ON_BLOCK_BEGIN,
//		0xff0000, //CMD_ON_BLOCK_END
//		0xff0000, //CMD_ON_BLOCK_END_LOOP
//		0xff00df, //CMD_ON_BLOCK_MFORWARD
//		0x00ff4a, //CMD_ON_BLOCK_MBACKWARD
//                0xff0000,//CMD_ON_BLOCK_TURNLEFT
//                0xfff100,//CMD_ON_BLOCK_TURNRIGHT
//                0x0006ff//CMD_ON_BLOCK_UNKNOW
//		 };

#define I2S_BUFFER_SIZE (3*NLEDS + RESET_BITS)
APP_TIMER_DEF(m_repeated_timer_led);     /**< Handler for repeated timer used to blink LED 1. */

void ws2812_init();
uint32_t caclChannelValue(uint8_t level);
void ws_set(uint8_t red,uint8_t green,uint8_t blue);
void ws_show();
void ws_off();
void ws_set_show(uint8_t red,uint8_t green,uint8_t blue);
void ws_set_show_cmd(cmd_evt_t p_cmd_evt);
void ws_device_show(const device_evt_t evt,const void * param);
void ws_set_show_32(uint32_t color);
uint32_t ColorHSV(uint16_t hue, uint8_t sat, uint8_t val);
#endif