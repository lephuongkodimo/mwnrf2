#include "montes_c.h"
#include "app_error.h"
#include "nrf_log.h"
static void gatt_error_handler(uint32_t   nrf_error,
                               void     * p_ctx,
                               uint16_t   conn_handle)
{
    ble_monte_blocks_c_t * p_ble_monte_blocks_c = (ble_monte_blocks_c_t *)p_ctx;

    NRF_LOG_DEBUG("A GATT Client error has occurred on conn_handle: 0X%X", conn_handle);

    if (p_ble_monte_blocks_c->error_handler != NULL)
    {
        p_ble_monte_blocks_c->error_handler(nrf_error);
    }
}
void ble_monte_blocks_c_on_db_disc_evt(ble_monte_blocks_c_t * p_ble_monte_blocks_c, ble_db_discovery_evt_t * p_evt)
{
    NRF_LOG_INFO("ble_monte_blocks_c_on_db_disc_evt:%d\n",p_evt->evt_type);
    ble_monte_blocks_c_evt_t monte_blocks_c_evt;
    memset(&monte_blocks_c_evt,0,sizeof(ble_monte_blocks_c_evt_t));

    ble_gatt_db_char_t * p_chars = p_evt->params.discovered_db.charateristics;

    // Check if the NUS was discovered.
    if (    (p_evt->evt_type == BLE_DB_DISCOVERY_COMPLETE)
        &&  (p_evt->params.discovered_db.srv_uuid.uuid == BLE_UUID_BLOCKS_SERVICE)
        &&  (p_evt->params.discovered_db.srv_uuid.type == p_ble_monte_blocks_c->uuid_type))
    {
        for (uint32_t i = 0; i < p_evt->params.discovered_db.char_count; i++)
        {
            switch (p_chars[i].characteristic.uuid.uuid)
            {
                case BLE_UUID_BLOCKS_BLOCK_CHARACTERISTIC:
                    monte_blocks_c_evt.handles.blocks_block_handle = p_chars[i].characteristic.handle_value;
                    break;

                case BLE_UUID_BLOCKS_PROGRAM_CHARACTERISTIC:
                    monte_blocks_c_evt.handles.blocks_program_handle = p_chars[i].characteristic.handle_value;
//                    monte_blocks_c_evt.handles.blocks_program_cccd_handle = p_chars[i].cccd_handle;
                    break;

                default:
                    break;
            }
        }
        if (p_ble_monte_blocks_c->evt_handler != NULL)
        {
            monte_blocks_c_evt.conn_handle = p_evt->conn_handle;
            monte_blocks_c_evt.evt_type    = BLE_MONTE_BLOCKS_C_EVT_DISCOVERY_COMPLETE;
            p_ble_monte_blocks_c->evt_handler(p_ble_monte_blocks_c, &monte_blocks_c_evt);
        }
    }
}

uint32_t ble_monte_blocks_c_init(ble_monte_blocks_c_t * p_ble_monte_blocks_c, ble_monte_blocks_c_init_t * p_ble_monte_blocks_c_init)
{
    uint32_t      err_code;
    ble_uuid_t    blocks_uuid;
    ble_uuid128_t monte_blocks_base_uuid = MONTE_BASE_UUID;

    VERIFY_PARAM_NOT_NULL(p_ble_monte_blocks_c);
    VERIFY_PARAM_NOT_NULL(p_ble_monte_blocks_c_init);
    VERIFY_PARAM_NOT_NULL(p_ble_monte_blocks_c_init->p_gatt_queue);
//p_ble_monte_blocks_c->uuid_type = BLE_UUID_TYPE_VENDOR_BEGIN;
    err_code = sd_ble_uuid_vs_add(&monte_blocks_base_uuid, &p_ble_monte_blocks_c->uuid_type);
    APP_ERROR_CHECK(err_code);
//    VERIFY_SUCCESS(err_code);

    blocks_uuid.type = p_ble_monte_blocks_c->uuid_type;
    blocks_uuid.uuid = BLE_UUID_BLOCKS_SERVICE;
    NRF_LOG_WARNING("p_ble_monte_blocks_c->uuid_type: %d",p_ble_monte_blocks_c->uuid_type);
    p_ble_monte_blocks_c->conn_handle           = BLE_CONN_HANDLE_INVALID;
    p_ble_monte_blocks_c->evt_handler           = p_ble_monte_blocks_c_init->evt_handler;
    p_ble_monte_blocks_c->error_handler         = p_ble_monte_blocks_c_init->error_handler;
    p_ble_monte_blocks_c->handles.blocks_program_handle = BLE_GATT_HANDLE_INVALID;
    p_ble_monte_blocks_c->handles.blocks_block_handle = BLE_GATT_HANDLE_INVALID;
    p_ble_monte_blocks_c->p_gatt_queue          = p_ble_monte_blocks_c_init->p_gatt_queue;

    return ble_db_discovery_evt_register(&blocks_uuid);//discovery service that we interested, looking flow uuid 
}

void ble_monte_blocks_c_on_ble_evt(ble_evt_t const * p_ble_evt, void * p_context)
{
    ble_monte_blocks_c_t * p_ble_monte_blocks_c = (ble_monte_blocks_c_t *)p_context;

    if ((p_ble_monte_blocks_c == NULL) || (p_ble_evt == NULL))
    {
        return;
    }

    if ( (p_ble_monte_blocks_c->conn_handle == BLE_CONN_HANDLE_INVALID)
       ||(p_ble_monte_blocks_c->conn_handle != p_ble_evt->evt.gap_evt.conn_handle)//<- giong nhu 1 dang quet handlle, cai nao dung voi cai characteristic quan tam thi moi xu lys tiep
       )
    {
        return;
    }

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GATTC_EVT_HVX: //evt recive notufy from WD
//            on_hvx(p_ble_monte_blocks_c, p_ble_evt);
            break;
        case BLE_GATTC_EVT_WRITE_RSP:
            printf("BLE_GATTC_EVT_WRITE_RSP\n");
            break;
        case BLE_GAP_EVT_DISCONNECTED:
            if (p_ble_evt->evt.gap_evt.conn_handle == p_ble_monte_blocks_c->conn_handle
                    && p_ble_monte_blocks_c->evt_handler != NULL)
            {
                ble_monte_blocks_c_evt_t monte_blocks_c_evt;

                monte_blocks_c_evt.evt_type = BLE_MONTE_BLOCKS_C_EVT_DISCONNECTED;

                p_ble_monte_blocks_c->conn_handle = BLE_CONN_HANDLE_INVALID;
                p_ble_monte_blocks_c->evt_handler(p_ble_monte_blocks_c, &monte_blocks_c_evt);
            }
            break;

        default:
            // No implementation needed.
            break;
    }
}

uint32_t ble_monte_blocks_c_string_send(ble_monte_blocks_c_t * p_ble_monte_blocks_c, uint8_t * p_string, uint16_t length)
{
    VERIFY_PARAM_NOT_NULL(p_ble_monte_blocks_c);

    nrf_ble_gq_req_t write_req;

    memset(&write_req, 0, sizeof(nrf_ble_gq_req_t));

    if (length > BLE_MONTE_BLOCKS_MAX_DATA_LEN)
    {
        NRF_LOG_WARNING("Content too long.");
        return NRF_ERROR_INVALID_PARAM;
    }
    if (p_ble_monte_blocks_c->conn_handle == BLE_CONN_HANDLE_INVALID)
    {
        NRF_LOG_WARNING("Connection handle invalid.");
        return NRF_ERROR_INVALID_STATE;
    }

    write_req.type                        = NRF_BLE_GQ_REQ_GATTC_WRITE;
    write_req.error_handler.cb            = gatt_error_handler;
    write_req.error_handler.p_ctx         = p_ble_monte_blocks_c;
    write_req.params.gattc_write.handle   = p_ble_monte_blocks_c->handles.blocks_program_handle;
    write_req.params.gattc_write.len      = length;
    write_req.params.gattc_write.offset   = 0;
    write_req.params.gattc_write.p_value  = p_string;
    write_req.params.gattc_write.write_op = BLE_GATT_OP_WRITE_CMD;
    write_req.params.gattc_write.flags    = BLE_GATT_EXEC_WRITE_FLAG_PREPARED_WRITE;

    return nrf_ble_gq_item_add(p_ble_monte_blocks_c->p_gatt_queue, &write_req, p_ble_monte_blocks_c->conn_handle);
}
//khi vu acomplete discovery thi phai kieu nhu la luu lai cai handle do vo thanh cua tao. sau nay chi can goi la xai duoc.
uint32_t ble_monte_blocks_c_handles_assign(ble_monte_blocks_c_t               * p_ble_monte_blocks,
                                  uint16_t                    conn_handle,
                                  ble_monte_blocks_c_handles_t const * p_peer_handles)
{
    VERIFY_PARAM_NOT_NULL(p_ble_monte_blocks);

    p_ble_monte_blocks->conn_handle = conn_handle;
    if (p_peer_handles != NULL)
    {
        p_ble_monte_blocks->handles.blocks_program_handle = p_peer_handles->blocks_program_handle;
        p_ble_monte_blocks->handles.blocks_block_handle      = p_peer_handles->blocks_block_handle;
//        p_ble_monte_blocks->handles.blocks_program_cccd_handle      = p_peer_handles->blocks_program_cccd_handle;
    }
    return nrf_ble_gq_conn_handle_register(p_ble_monte_blocks->p_gatt_queue, conn_handle);
}