#include "nrf_ws2812.h"
static nrf_mtx_t            m_led_mtx;       /**< Module API lock.*/
APP_TIMER_DEF(m_repeated_timer_id);     /**< Handler for repeated timer used to blink LED 1. */
static uint32_t mm_buffer_tx[I2S_BUFFER_SIZE];
static void repeated_timer_handler(void * p_context)
{
}

static void data_handler(nrf_drv_i2s_buffers_t const * p_released,
                         uint32_t                      status)
{
    // 'nrf_drv_i2s_next_buffers_set' is called directly from the handler
    // each time next buffers are requested, so data corruption is not
    // expected.
    ASSERT(p_released);

    // When the handler is called after the transfer has been stopped
    // (no next buffers are needed, only the used buffers are to be
    // released), there is nothing to do.
    if (!(status & NRFX_I2S_STATUS_NEXT_BUFFERS_NEEDED))
    {
        return;
    }

    // First call of this handler occurs right after the transfer is started.
    // No data has been transferred yet at this point, so there is nothing to
    // check. Only the buffers for the next part of the transfer should be
    // provided.
    if (!p_released->p_rx_buffer)
    {
//        nrf_drv_i2s_buffers_t const next_buffers = {
//            .p_rx_buffer = m_buffer_rx[1],
//            .p_tx_buffer = m_buffer_tx[1],
//        };
//        APP_ERROR_CHECK(nrf_drv_i2s_next_buffers_set(&next_buffers));
//
//        mp_block_to_fill = m_buffer_tx[1];
//done_transferred = true;
//NRF_LOG_INFO("p_tx_buffer ");
    }
    else
    {
//    NRF_LOG_INFO("p_rx_buffer ");
//        mp_block_to_check = p_released->p_rx_buffer;
//        // The driver has just finished accessing the buffers pointed by
//        // 'p_released'. They can be used for the next part of the transfer
//        // that will be scheduled now.
//        APP_ERROR_CHECK(nrf_drv_i2s_next_buffers_set(p_released));
//
//        // The pointer needs to be typecasted here, so that it is possible to
//        // modify the content it is pointing to (it is marked in the structure
//        // as pointing to constant data because the driver is not supposed to
//        // modify the provided data).
//        mp_block_to_fill = (uint32_t *)p_released->p_tx_buffer;
    }
}
static bool ble_device_connected = false;
static uint8_t hsv_index = 0 ;
static bool select_color =false;
static bool is_running_timer_led =false ;
static void repeated_timer_led_handler(void * p_context)
{
//buzzer_run(NOTE_C,100);
//tag_read_process_allow= true;
int* evt = (int*) p_context;
    if(evt){
     if(select_color) {
       hsv_index = hsv_index-5;
       ws_set_show(hsv_index,0,0); 
       if(hsv_index==10){select_color =0;}
      }
      else{
        hsv_index = hsv_index+10;
       ws_set_show(hsv_index,0,0); 
       if(hsv_index==200){select_color=1;}
      }
    } else{
    if(select_color) {
      ws_set_show(250,0,0); 
      }else{
      ws_set_show(0,250,0);
    }
    select_color= !select_color;
    }
}
void ws2812_init(){
    nrf_drv_i2s_config_t config = NRF_DRV_I2S_DEFAULT_CONFIG;
    //    config.sck_pin      = NRF_DRV_I2S_PIN_NOT_USED;
    //    config.lrck_pin   = NRF_DRV_I2S_PIN_NOT_USED;
    config.sdin_pin  = NRF_DRV_I2S_PIN_NOT_USED;
    config.mck_pin = NRF_DRV_I2S_PIN_NOT_USED;
    config.sdout_pin = NRF_WS2812_PIN;
    config.mck_setup = NRF_I2S_MCK_32MDIV10;
    config.ratio     = NRF_I2S_RATIO_32X;
    config.channels  = NRF_I2S_CHANNELS_STEREO;
    uint32_t err_code = nrf_drv_i2s_init(&config, data_handler);
    APP_ERROR_CHECK(err_code);
//    // Create timers
//    err_code = app_timer_create(&m_repeated_timer_id,
//                                APP_TIMER_MODE_REPEATED,
//                                repeated_timer_handler);
//    APP_ERROR_CHECK(err_code);
//    err_code = app_timer_start(m_repeated_timer_id, APP_TIMER_TICKS(100), NULL);
//    APP_ERROR_CHECK(err_code);

              err_code = app_timer_create(&m_repeated_timer_led,
                                APP_TIMER_MODE_REPEATED,
                                repeated_timer_led_handler);
              APP_ERROR_CHECK(err_code);
              nrf_mtx_init(&m_led_mtx);
                  int red = 0;
            //ws_set_show(250,0,0);
            uint16_t evt = BLE_GAP_EVT_DISCONNECTED;
            ws_device_show(BLE_DEVICE_STT,&evt);
}


uint32_t caclChannelValue(uint8_t level)
{
    uint32_t val = 0;

    // 0 
    if(level == 0) {
        val = 0x88888888;
    }
    // 255
    else if (level == 255) {
        val = 0xeeeeeeee;
    }
    else {
        // apply 4-bit 0xe HIGH pattern wherever level bits are 1.
        val = 0x88888888;
        for (uint8_t i = 0; i < 8; i++) {
            if((1 << i) & level) {
                uint32_t mask = ~(0x0f << 4*i);
                uint32_t patt = (0x0e << 4*i);
                val = (val & mask) | patt;
            }
        }

        // swap 16 bits
        val = (val >> 16) | (val << 16);
    }

    return val;
}

void ws_show(){
        // start I2S
        if (!nrf_mtx_trylock(&m_led_mtx))
        {
            return;
        }
        nrf_drv_i2s_buffers_t const initial_buffers = {
          .p_tx_buffer = mm_buffer_tx,
          .p_rx_buffer = NULL,
        };
         uint32_t err_code = nrf_drv_i2s_start(&initial_buffers, I2S_BUFFER_SIZE, 0);
         APP_ERROR_CHECK(err_code);
         nrf_delay_us((NLEDS+20) * (24*5/4));
         nrf_drv_i2s_stop();
         nrf_mtx_unlock(&m_led_mtx);
}
void ws_set(uint8_t red,uint8_t green,uint8_t blue){
            int i = NLEDS-1;
        mm_buffer_tx[i]   =   caclChannelValue(green);
        mm_buffer_tx[i+1] =   caclChannelValue(red);
        mm_buffer_tx[i+2] =   caclChannelValue(blue);
}
//function hadle all setup color, stt from evt

void ws_device_show(const device_evt_t evt,const void * param){
 NRF_LOG_INFO("ws_device_show:%d",evt);
        switch(evt){
          case PRESENT_CMD:{
            cmd_evt_t* p_cmd_evt = (cmd_evt_t*) param;
            ws_set_show_cmd(*p_cmd_evt);
            }
            break;
            case RESET_LED:{
              if(is_running_timer_led) {
                app_timer_stop(m_repeated_timer_led);
                is_running_timer_led=false;                
              }
            }break;
          case PROGRAM_STT:{ //<-Trang thai cua program
            cmd_evt_t* p_cmd_evt = (cmd_evt_t*) param;   
            switch(*p_cmd_evt){
              case CMD_ON_COMPLETED_PROGRAM:{
              if(is_running_timer_led) {
                app_timer_stop(m_repeated_timer_led);
                is_running_timer_led=false;                
              }
              ws_set_show(250,0,0);
              uint32_t err_code = app_timer_start(m_repeated_timer_led, APP_TIMER_TICKS(500), NULL);
              APP_ERROR_CHECK(err_code);
              is_running_timer_led = true;
              }
              //TODO
              break;
              case CMD_SEND_COMPLETED:
              {
                if(is_running_timer_led) {
                app_timer_stop(m_repeated_timer_led);
                is_running_timer_led=false;                
                }
                ws_set_show(0,250,0);
              }
              break;
              default:
              break;
            }     
          }break;
          case BLE_DEVICE_STT: { //<- cac trang thai cua BLE
            uint16_t* evt_id = (uint16_t*) param;
            NRF_LOG_INFO("BLE_DEVICE_STT:%d",*evt_id);
            if(is_running_timer_led) {
                    app_timer_stop(m_repeated_timer_led);
                    is_running_timer_led=false;                
            }
            switch(*evt_id){
              case BLE_GAP_EVT_CONNECTED:{
                ws_set_show(0,250,0);
                }
              break;
              case BLE_GAP_EVT_DISCONNECTED:{
//                ws_set_show(250,0,0);
                  ble_device_connected = false;
                  uint32_t err_code = app_timer_start(m_repeated_timer_led, APP_TIMER_TICKS(20),evt_id);
                  APP_ERROR_CHECK(err_code);
                  is_running_timer_led = true;
                  }
              break;
              default:
              break;
            }
          }break;
          case COLOR_OPTION:{ //<-tuy chin color
              uint32_t *color= (uint32_t*) param;
               NRF_LOG_INFO("COLOR_OPTION:%d",*color);
              ws_set_show_32(*color);}
              break;
          default:
          break;
        }
}
//BGR
uint32_t colour_32[4] = {
		0x00cc7a,
		0xfff100,
//		0x20062f,
		0xff00df,
		0x00ff4a
		 };
int index = 0;
void ws_set_show_cmd(cmd_evt_t p_cmd_evt){
uint32_t color;
index++;
index = index%4 ;

//    uint8_t green = 0;
//    uint8_t red = 0 ;
//    uint8_t blue = 0;
  switch(p_cmd_evt){
    case CMD_ON_BLOCK_BEGIN:
//        green = 200;
color=0x00ff00;
        break;
    case CMD_ON_BLOCK_END:
color=0x0000ff;
        break;
    case CMD_ON_BLOCK_END_LOOP:
color=0x0000ff;
        break;
    case CMD_ON_BLOCK_MFORWARD:
color = colour_32[index];
      break;
    case CMD_ON_BLOCK_MBACKWARD:
color =  colour_32[index];
      break;
    case CMD_ON_BLOCK_TURNLEFT:
color =  colour_32[index];
      break;
    case CMD_ON_BLOCK_TURNRIGHT:
color =  colour_32[index];
      break;
    default:
      break;
  }
//  ws_set_show(red,green,blue);
 NRF_LOG_INFO("index:%d p_cmd_evt:%d color:%d",index,p_cmd_evt,color);
  ws_set_show_32(color);
}
void ws_set_show(uint8_t red,uint8_t green,uint8_t blue){
      ws_set(red,green,blue);
      ws_show();
}
void ws_set_show_32(uint32_t color){
      uint8_t colorR = color & 0xff;
      uint8_t colorG = (color & 0xff00) >> 8;
      uint8_t colorB =  (color & 0xff0000) >> 16;
      ws_set(colorR,colorG,colorB);
      ws_show();
}
void ws_off(){
      ws_set(0,0,0);
      ws_show();
}
uint32_t ColorHSV(uint16_t hue, uint8_t sat, uint8_t val) {

  uint8_t r, g, b;

  // Remap 0-65535 to 0-1529. Pure red is CENTERED on the 64K rollover;
  // 0 is not the start of pure red, but the midpoint...a few values above
  // zero and a few below 65536 all yield pure red (similarly, 32768 is the
  // midpoint, not start, of pure cyan). The 8-bit RGB hexcone (256 values
  // each for red, green, blue) really only allows for 1530 distinct hues
  // (not 1536, more on that below), but the full unsigned 16-bit type was
  // chosen for hue so that one's code can easily handle a contiguous color
  // wheel by allowing hue to roll over in either direction.
  hue = (hue * 1530L + 32768) / 65536;
  // Because red is centered on the rollover point (the +32768 above,
  // essentially a fixed-point +0.5), the above actually yields 0 to 1530,
  // where 0 and 1530 would yield the same thing. Rather than apply a
  // costly modulo operator, 1530 is handled as a special case below.

  // So you'd think that the color "hexcone" (the thing that ramps from
  // pure red, to pure yellow, to pure green and so forth back to red,
  // yielding six slices), and with each color component having 256
  // possible values (0-255), might have 1536 possible items (6*256),
  // but in reality there's 1530. This is because the last element in
  // each 256-element slice is equal to the first element of the next
  // slice, and keeping those in there this would create small
  // discontinuities in the color wheel. So the last element of each
  // slice is dropped...we regard only elements 0-254, with item 255
  // being picked up as element 0 of the next slice. Like this:
  // Red to not-quite-pure-yellow is:        255,   0, 0 to 255, 254,   0
  // Pure yellow to not-quite-pure-green is: 255, 255, 0 to   1, 255,   0
  // Pure green to not-quite-pure-cyan is:     0, 255, 0 to   0, 255, 254
  // and so forth. Hence, 1530 distinct hues (0 to 1529), and hence why
  // the constants below are not the multiples of 256 you might expect.

  // Convert hue to R,G,B (nested ifs faster than divide+mod+switch):
  if(hue < 510) {         // Red to Green-1
    b = 0;
    if(hue < 255) {       //   Red to Yellow-1
      r = 255;
      g = hue;            //     g = 0 to 254
    } else {              //   Yellow to Green-1
      r = 510 - hue;      //     r = 255 to 1
      g = 255;
    }
  } else if(hue < 1020) { // Green to Blue-1
    r = 0;
    if(hue <  765) {      //   Green to Cyan-1
      g = 255;
      b = hue - 510;      //     b = 0 to 254
    } else {              //   Cyan to Blue-1
      g = 1020 - hue;     //     g = 255 to 1
      b = 255;
    }
  } else if(hue < 1530) { // Blue to Red-1
    g = 0;
    if(hue < 1275) {      //   Blue to Magenta-1
      r = hue - 1020;     //     r = 0 to 254
      b = 255;
    } else {              //   Magenta to Red-1
      r = 255;
      b = 1530 - hue;     //     b = 255 to 1
    }
  } else {                // Last 0.5 Red (quicker than % operator)
    r = 255;
    g = b = 0;
  }

  // Apply saturation and value to R,G,B, pack into 32-bit result:
  uint32_t v1 =   1 + val; // 1 to 256; allows >>8 instead of /255
  uint16_t s1 =   1 + sat; // 1 to 256; same reason
  uint8_t  s2 = 255 - sat; // 255 to 0
  ws_set_show((((r * s1) >> 8) + s2) * v1,((((g * s1) >> 8) + s2) * v1),((((b * s1) >> 8) + s2) * v1));
//  return ((((((r * s1) >> 8) + s2) * v1) & 0xff00) >> 8) |
//          (((((g * s1) >> 8) + s2) * v1) & 0xff00)       |
//         ( ((((b * s1) >> 8) + s2) * v1)           << 8);
}