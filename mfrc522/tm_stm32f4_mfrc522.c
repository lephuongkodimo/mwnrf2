/**	
 * |----------------------------------------------------------------------
 * | Copyright (C) Tilen Majerle, 2014
 * | 
 * | This program is free software: you can redistribute it and/or modify
 * | it under the terms of the GNU General Public License as published by
 * | the Free Software Foundation, either version 3 of the License, or
 * | any later version.
 * |  
 * | This program is distributed in the hope that it will be useful,
 * | but WITHOUT ANY WARRANTY; without even the implied warranty of
 * | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * | GNU General Public License for more details.
 * | 
 * | You should have received a copy of the GNU General Public License
 * | along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * |----------------------------------------------------------------------
 */
#include "tm_stm32f4_mfrc522.h"
#define SPI_INSTANCE  0 /**< SPI instance index. */
static const nrf_drv_spi_t spi = NRF_DRV_SPI_INSTANCE(SPI_INSTANCE);  /**< SPI instance. */
static volatile bool spi_xfer_done;  /**< Flag used to indicate that SPI instance completed the transfer. */
static uint8_t       m_rx_buf[1];    /**< RX buffer. */
typedef  uint8_t byte; 
static void spi_event_handler(nrf_drv_spi_evt_t const * p_event,
                       void *                    p_context)
{
    spi_xfer_done = true;
//    NRF_LOG_INFO("Transfer completed.");
//    if (m_rx_buf[0] != 0)
//    {
//        NRF_LOG_INFO(" Received:");
//        NRF_LOG_HEXDUMP_INFO(m_rx_buf,2);
//    }
}
void TM_MFRC522_Init(void) {

      nrf_gpio_cfg_output(SPI_SS_PIN);
      nrf_drv_spi_config_t spi_config = NRF_DRV_SPI_DEFAULT_CONFIG;
      spi_config.ss_pin   = SPI_SS_PIN;
      spi_config.miso_pin = SPI_MISO_PIN;
      spi_config.mosi_pin = SPI_MOSI_PIN;
      spi_config.sck_pin  = 28;
      ret_code_t err = nrf_drv_spi_init(&spi, &spi_config, spi_event_handler, NULL);
      debug_info("err:%d",err);
//	TM_MFRC522_Reset();
        uint8_t vl = TM_MFRC522_ReadRegister(MFRC522_REG_VERSION);
        debug_info("MFRC522_REG_VERSION %x\n",vl);

	TM_MFRC522_WriteRegister(MFRC522_REG_T_MODE, 0x8D);
        vl = TM_MFRC522_ReadRegister(MFRC522_REG_T_MODE);
        debug_info("MFRC522_REG_T_MODE %x\n",vl);
	TM_MFRC522_WriteRegister(MFRC522_REG_T_PRESCALER, 0x3E);
        vl = TM_MFRC522_ReadRegister(MFRC522_REG_T_PRESCALER);
        debug_info("MFRC522_REG_T_PRESCALER %x\n",vl);
	TM_MFRC522_WriteRegister(MFRC522_REG_T_RELOAD_L, 0x38); 
       vl = TM_MFRC522_ReadRegister(MFRC522_REG_T_RELOAD_L);
        debug_info("MFRC522_REG_T_RELOAD_L %x\n",vl);
	TM_MFRC522_WriteRegister(MFRC522_REG_T_RELOAD_H, 0);
        vl = TM_MFRC522_ReadRegister(MFRC522_REG_T_RELOAD_H);
        debug_info("MFRC522_REG_T_RELOAD_H %x\n",vl);

	/* 48dB gain */
	TM_MFRC522_WriteRegister(MFRC522_REG_RF_CFG, 0x70);
        TM_MFRC522_WriteRegister(MFRC522_REG_TX_AUTO, 0x40);
        vl = TM_MFRC522_ReadRegister(MFRC522_REG_RF_CFG);
        debug_info("MFRC522_REG_RF_CFG %x\n",vl);

	vl = TM_MFRC522_ReadRegister(MFRC522_REG_VERSION);
        debug_info("MFRC522_REG_VERSION %x\n",vl);

	
        vl = TM_MFRC522_ReadRegister(MFRC522_REG_TX_AUTO);
        debug_info("MFRC522_REG_RF_CFG %x\n",vl);

	TM_MFRC522_WriteRegister(MFRC522_REG_MODE, 0x3D);
        vl = TM_MFRC522_ReadRegister(MFRC522_REG_MODE);
        debug_info("MFRC522_REG_RF_CFG %x\n",vl);
	TM_MFRC522_AntennaOn();		//Open the antenna

        vl = TM_MFRC522_ReadRegister(MFRC522_REG_VERSION);
        debug_info("MFRC522_REG_VERSION %x\n",vl);
        vl = TM_MFRC522_ReadRegister(MFRC522_REG_VERSION);
        debug_info("MFRC522_REG_VERSION %x\n",vl);
}

TM_MFRC522_Status_t TM_MFRC522_Check(uint8_t* id) {
	TM_MFRC522_Status_t status;
	//Find cards, return card type
        	// Reset baud rates
//	TM_MFRC522_WriteRegister(MFRC522_REG_TX_MODE, 0x00);
//	TM_MFRC522_WriteRegister(MFRC522_REG_RX_MODE, 0x00);
//	// Reset ModWidthReg
//	TM_MFRC522_WriteRegister(MFRC522_REG_MOD_WIDTH, 0x26);
//        debug_info("TM_MFRC522_Check");
	status = TM_MFRC522_Request(PICC_REQIDL, id);	
//        debug_info("stttttt:%d",status);
	if (status == MI_OK) {
        
		//Card detected
		//Anti-collision, return card serial number 4 bytes
		status = TM_MFRC522_Anticoll(id);
//                uint8_t size  = TM_MFRC522_SelectTag(id);        
//                if(size!=0){
//                debug_info("size=:%d",size);
//                status = MI_OK;
//                }
	}
//	TM_MFRC522_Halt();			//Command card into hibernation 

	return status;
}
TM_MFRC522_Status_t TM_MFRC522_Check_debug(uint8_t* id) {
	TM_MFRC522_Status_t status;
	//Find cards, return card type
        	// Reset baud rates
//	TM_MFRC522_WriteRegister(MFRC522_REG_TX_MODE, 0x00);
//	TM_MFRC522_WriteRegister(MFRC522_REG_RX_MODE, 0x00);
//	// Reset ModWidthReg
//	TM_MFRC522_WriteRegister(MFRC522_REG_MOD_WIDTH, 0x26);
        debug_info("TM_MFRC522_Check");
	status = TM_MFRC522_Request_debug(PICC_REQIDL, id);	
        debug_info("stt:%d",status);
	if (status == MI_OK) {
        
		//Card detected
		//Anti-collision, return card serial number 4 bytes
		status = TM_MFRC522_Anticoll(id);
//                uint8_t size  = TM_MFRC522_SelectTag(id);        
//                if(size!=0){
//                debug_info("size=:%d",size);
//                status = MI_OK;
//                }
	}
//	TM_MFRC522_Halt();			//Command card into hibernation 

	return status;
}

TM_MFRC522_Status_t TM_MFRC522_Compare(uint8_t* CardID, uint8_t* CompareID) {
	uint8_t i;
	for (i = 0; i < 5; i++) {
		if (CardID[i] != CompareID[i]) {
			return MI_ERR;
		}
	}
	return MI_OK;
}

//void TM_MFRC522_InitPins(void) {
//	GPIO_InitTypeDef GPIO_InitStruct;
//	//Enable clock
//	RCC_AHB1PeriphClockCmd(MFRC522_CS_RCC, ENABLE);
//
//	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
//	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
//	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
//	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
//	//CS pin
//	GPIO_InitStruct.GPIO_Pin = MFRC522_CS_PIN;
//	GPIO_Init(MFRC522_CS_PORT, &GPIO_InitStruct);	
//
//	MFRC522_CS_HIGH;
//}

void TM_MFRC522_WriteRegister(uint8_t addr, uint8_t val) {
	//CS low
//	MFRC522_CS_LOW;
	//Send address
//	TM_SPI_Send(MFRC522_SPI, (addr << 1) & 0x7E);
        uint8_t       m_tx_buf[2];
        m_tx_buf[0] = (addr << 1) & 0x7E;
        m_tx_buf[1] = val;
        spi_xfer_done = false;

        nrf_drv_spi_transfer(&spi,&m_tx_buf[0], 2, NULL, 0);

        while (!spi_xfer_done)
        {
            __WFE();
        }
        NRF_LOG_FLUSH();
        //        m_tx_buf[1] = val;
//        spi_xfer_done = false;
//
//        nrf_drv_spi_transfer(&spi,&m_tx_buf[1], 1, NULL, 0);
//
//        while (!spi_xfer_done)
//        {
//            __WFE();
//        }
	//Send data	
//	TM_SPI_Send(MFRC522_SPI, val);
	//CS high
//	MFRC522_CS_HIGH;
}

uint8_t TM_MFRC522_ReadRegister(uint8_t addr) {

	//CS low
//	MFRC522_CS_LOW;

//	TM_SPI_Send(MFRC522_SPI, ((addr << 1) & 0x7E) | 0x80);	
//	val = TM_SPI_Send(MFRC522_SPI, MFRC522_DUMMY);

        uint8_t _reg = ((addr << 1) & 0x7E) | 0x80 ;
        memset(m_rx_buf, 0, 1);
        spi_xfer_done = false;
    
        nrf_drv_spi_transfer(&spi,&_reg, 1, m_rx_buf,1);

        while (!spi_xfer_done)
        {
            __WFE();
        }
        NRF_LOG_FLUSH();
        memset(m_rx_buf, 0, 1);
        spi_xfer_done = false;
        uint8_t m_reg = 0x00;
        nrf_drv_spi_transfer(&spi,&m_reg, 1, m_rx_buf,1);

        while (!spi_xfer_done)
        {
            __WFE();
        }
        NRF_LOG_FLUSH();
	//CS high
//	MFRC522_CS_HIGH;

	return m_rx_buf[0];	
}
//uint8_t TM_MFRC522_ReadRegister(uint8_t addr)
//{
//    uint8_t ucResult=0;
//    uint8_t ucAddr;
//    uint8_t tx_data[2]={0x00,0x00};
//    memset(m_rx_buf, 0, 2);
//    ucAddr = ((addr << 1) & 0x7E) | 0x80;
//    spi_xfer_done = false;
//    tx_data[0] = ucAddr;
//    nrf_drv_spi_transfer(&spi,&tx_data[0], 1, &m_rx_buf[0],1);
//
//    while (!spi_xfer_done)
//    {
//        __WFE();
//    }
//    ucResult = m_rx_buf[0];
//    return ucResult;
//}
//void TM_MFRC522_WriteRegister(uint8_t addr, uint8_t val)
//{  
//                uint8_t tx_data[2]={0x00,0x00};
////                m_rx_buf[2]={0x00,0x00};
//                memset(m_rx_buf, 0, 2);
//                uint8_t ucAddr;
//                ucAddr = ((addr << 1) & 0x7E);
//                tx_data[0] = ucAddr;
//                tx_data[1] = val;
//                spi_xfer_done = false;
//                nrf_drv_spi_transfer(&spi,&tx_data[0], 2, &m_rx_buf[0],2);
//                    while (!spi_xfer_done)
//                {
//                    __WFE();
//                }
//}
void TM_MFRC522_SetBitMask(uint8_t reg, uint8_t mask) {
	TM_MFRC522_WriteRegister(reg, TM_MFRC522_ReadRegister(reg) | mask);
}

void TM_MFRC522_ClearBitMask(uint8_t reg, uint8_t mask){
	TM_MFRC522_WriteRegister(reg, TM_MFRC522_ReadRegister(reg) & (~mask));
} 

void TM_MFRC522_AntennaOn(void) {
	uint8_t temp;

	temp = TM_MFRC522_ReadRegister(MFRC522_REG_TX_CONTROL);
	if (!(temp & 0x03)) {
		TM_MFRC522_SetBitMask(MFRC522_REG_TX_CONTROL, 0x03);
	}
}

void TM_MFRC522_AntennaOff(void) {
	TM_MFRC522_ClearBitMask(MFRC522_REG_TX_CONTROL, 0x03);
}

void TM_MFRC522_Reset(void) {
	TM_MFRC522_WriteRegister(MFRC522_REG_COMMAND, PCD_RESETPHASE);
}

TM_MFRC522_Status_t TM_MFRC522_Request(uint8_t reqMode, uint8_t* TagType) {
	TM_MFRC522_Status_t status;  
	uint16_t backBits;			//The received data bits

	TM_MFRC522_WriteRegister(MFRC522_REG_BIT_FRAMING, 0x07);		//TxLastBists = BitFramingReg[2..0]	???

	TagType[0] = reqMode;
	status = TM_MFRC522_ToCard(PCD_TRANSCEIVE, TagType, 1, TagType, &backBits);

	if ((status != MI_OK) || (backBits != 0x10)) {    
		status = MI_ERR;
	}

	return status;
}
TM_MFRC522_Status_t TM_MFRC522_Request_debug(uint8_t reqMode, uint8_t* TagType) {
	TM_MFRC522_Status_t status;  
	uint16_t backBits;			//The received data bits

	TM_MFRC522_WriteRegister(MFRC522_REG_BIT_FRAMING, 0x07);		//TxLastBists = BitFramingReg[2..0]	???

	TagType[0] = reqMode;
	status = TM_MFRC522_ToCard_debug(PCD_TRANSCEIVE, TagType, 1, TagType, &backBits);

	if ((status != MI_OK) || (backBits != 0x10)) {    
		status = MI_ERR;
	}

	return status;
}
TM_MFRC522_Status_t TM_MFRC522_ToCard_debug(uint8_t command, uint8_t* sendData, uint8_t sendLen, uint8_t* backData, uint16_t* backLen) {
	TM_MFRC522_Status_t status = MI_ERR;
	uint8_t irqEn = 0x00;
	uint8_t waitIRq = 0x00;
	uint8_t lastBits;
	uint8_t n;
	uint16_t i;

	switch (command) {
		case PCD_AUTHENT: {
			irqEn = 0x12;
			waitIRq = 0x10;
			break;
		}
		case PCD_TRANSCEIVE: {
			irqEn = 0x77;
			waitIRq = 0x30;
			break;
		}
		default:
			break;
	}

	TM_MFRC522_WriteRegister(MFRC522_REG_COMM_IE_N, irqEn | 0x80);
	TM_MFRC522_ClearBitMask(MFRC522_REG_COMM_IRQ, 0x80);
	TM_MFRC522_SetBitMask(MFRC522_REG_FIFO_LEVEL, 0x80);

	TM_MFRC522_WriteRegister(MFRC522_REG_COMMAND, PCD_IDLE);

	//Writing data to the FIFO
	for (i = 0; i < sendLen; i++) {   
		TM_MFRC522_WriteRegister(MFRC522_REG_FIFO_DATA, sendData[i]);    
	}

	//Execute the command
	TM_MFRC522_WriteRegister(MFRC522_REG_COMMAND, command);
	if (command == PCD_TRANSCEIVE) {    
		TM_MFRC522_SetBitMask(MFRC522_REG_BIT_FRAMING, 0x80);		//StartSend=1,transmission of data starts  
	}   

	//Waiting to receive data to complete
	i = 2000;	//i according to the clock frequency adjustment, the operator M1 card maximum waiting time 25ms???
	do {
		//CommIrqReg[7..0]
		//Set1 TxIRq RxIRq IdleIRq HiAlerIRq LoAlertIRq ErrIRq TimerIRq
		n = TM_MFRC522_ReadRegister(MFRC522_REG_COMM_IRQ);
                debug_info("n:%x\n",n);
                NRF_LOG_FLUSH();
		i--;
	} while ((i!=0) && !(n&0x01) && !(n&waitIRq));

	TM_MFRC522_ClearBitMask(MFRC522_REG_BIT_FRAMING, 0x80);			//StartSend=0

	if (i != 0)  {
		if (!(TM_MFRC522_ReadRegister(MFRC522_REG_ERROR) & 0x1B)) {
			status = MI_OK;
			if (n & irqEn & 0x01) {   
				status = MI_NOTAGERR;
			}

			if (command == PCD_TRANSCEIVE) {
				n = TM_MFRC522_ReadRegister(MFRC522_REG_FIFO_LEVEL);
				lastBits = TM_MFRC522_ReadRegister(MFRC522_REG_CONTROL) & 0x07;
				if (lastBits) {   
					*backLen = (n - 1) * 8 + lastBits;   
				} else {   
					*backLen = n * 8;   
				}

				if (n == 0) {   
					n = 1;    
				}
				if (n > MFRC522_MAX_LEN) {   
					n = MFRC522_MAX_LEN;   
				}

				//Reading the received data in FIFO
				for (i = 0; i < n; i++) {   
					backData[i] = TM_MFRC522_ReadRegister(MFRC522_REG_FIFO_DATA);    
				}
			}
		} else {   
			status = MI_ERR;  
		}
	}

	return status;
}
TM_MFRC522_Status_t TM_MFRC522_ToCard(uint8_t command, uint8_t* sendData, uint8_t sendLen, uint8_t* backData, uint16_t* backLen) {
	TM_MFRC522_Status_t status = MI_ERR;
	uint8_t irqEn = 0x00;
	uint8_t waitIRq = 0x00;
	uint8_t lastBits;
	uint8_t n;
	uint16_t i;

	switch (command) {
		case PCD_AUTHENT: {
			irqEn = 0x12;
			waitIRq = 0x10;
			break;
		}
		case PCD_TRANSCEIVE: {
			irqEn = 0x77;
			waitIRq = 0x30;
			break;
		}
		default:
			break;
	}

	TM_MFRC522_WriteRegister(MFRC522_REG_COMM_IE_N, irqEn | 0x80);
	TM_MFRC522_ClearBitMask(MFRC522_REG_COMM_IRQ, 0x80);
	TM_MFRC522_SetBitMask(MFRC522_REG_FIFO_LEVEL, 0x80);

	TM_MFRC522_WriteRegister(MFRC522_REG_COMMAND, PCD_IDLE);

	//Writing data to the FIFO
	for (i = 0; i < sendLen; i++) {   
		TM_MFRC522_WriteRegister(MFRC522_REG_FIFO_DATA, sendData[i]);    
	}

	//Execute the command
	TM_MFRC522_WriteRegister(MFRC522_REG_COMMAND, command);
	if (command == PCD_TRANSCEIVE) {    
		TM_MFRC522_SetBitMask(MFRC522_REG_BIT_FRAMING, 0x80);		//StartSend=1,transmission of data starts  
	}   

	//Waiting to receive data to complete
	i = 2000;	//i according to the clock frequency adjustment, the operator M1 card maximum waiting time 25ms???
	do {
		//CommIrqReg[7..0]
		//Set1 TxIRq RxIRq IdleIRq HiAlerIRq LoAlertIRq ErrIRq TimerIRq
		n = TM_MFRC522_ReadRegister(MFRC522_REG_COMM_IRQ);
//                debug_info("n:%x\n",n);
//                NRF_LOG_FLUSH();
		i--;
	} while ((i!=0) && !(n&0x01) && !(n&waitIRq));

	TM_MFRC522_ClearBitMask(MFRC522_REG_BIT_FRAMING, 0x80);			//StartSend=0

	if (i != 0)  {
		if (!(TM_MFRC522_ReadRegister(MFRC522_REG_ERROR) & 0x1B)) {
			status = MI_OK;
			if (n & irqEn & 0x01) {   
				status = MI_NOTAGERR;
			}

			if (command == PCD_TRANSCEIVE) {
				n = TM_MFRC522_ReadRegister(MFRC522_REG_FIFO_LEVEL);
				lastBits = TM_MFRC522_ReadRegister(MFRC522_REG_CONTROL) & 0x07;
				if (lastBits) {   
					*backLen = (n - 1) * 8 + lastBits;   
				} else {   
					*backLen = n * 8;   
				}

				if (n == 0) {   
					n = 1;    
				}
				if (n > MFRC522_MAX_LEN) {   
					n = MFRC522_MAX_LEN;   
				}

				//Reading the received data in FIFO
				for (i = 0; i < n; i++) {   
					backData[i] = TM_MFRC522_ReadRegister(MFRC522_REG_FIFO_DATA);    
				}
			}
		} else {   
			status = MI_ERR;  
		}
	}

	return status;
}

TM_MFRC522_Status_t TM_MFRC522_Anticoll(uint8_t* serNum) {
	TM_MFRC522_Status_t status;
	uint8_t i;
	uint8_t serNumCheck = 0;
	uint16_t unLen;

	TM_MFRC522_WriteRegister(MFRC522_REG_BIT_FRAMING, 0x00);		//TxLastBists = BitFramingReg[2..0]

	serNum[0] = PICC_ANTICOLL;
	serNum[1] = 0x20;
	status = TM_MFRC522_ToCard(PCD_TRANSCEIVE, serNum, 2, serNum, &unLen);

	if (status == MI_OK) {
		//Check card serial number
		for (i = 0; i < 4; i++) {   
			serNumCheck ^= serNum[i];
		}
		if (serNumCheck != serNum[i]) {   
			status = MI_ERR;    
		}
	}
	return status;
} 

void TM_MFRC522_CalculateCRC(uint8_t*  pIndata, uint8_t len, uint8_t* pOutData) {
	uint8_t i, n;

	TM_MFRC522_ClearBitMask(MFRC522_REG_DIV_IRQ, 0x04);			//CRCIrq = 0
	TM_MFRC522_SetBitMask(MFRC522_REG_FIFO_LEVEL, 0x80);			//Clear the FIFO pointer
	//Write_MFRC522(CommandReg, PCD_IDLE);

	//Writing data to the FIFO	
	for (i = 0; i < len; i++) {   
		TM_MFRC522_WriteRegister(MFRC522_REG_FIFO_DATA, *(pIndata+i));   
	}
	TM_MFRC522_WriteRegister(MFRC522_REG_COMMAND, PCD_CALCCRC);

	//Wait CRC calculation is complete
	i = 0xFF;
	do {
		n = TM_MFRC522_ReadRegister(MFRC522_REG_DIV_IRQ);
		i--;
                if(n&0x04){
                        TM_MFRC522_WriteRegister(MFRC522_REG_COMMAND, PCD_IDLE);	// Stop calculating CRC for new content in the FIFO.
                	pOutData[0] = TM_MFRC522_ReadRegister(MFRC522_REG_CRC_RESULT_L);
                        pOutData[1] = TM_MFRC522_ReadRegister(MFRC522_REG_CRC_RESULT_M);
                        return;
                }
	} while ((i!=0) && !(n&0x04));			//CRCIrq = 1

	//Read CRC calculation result
	pOutData[0] = TM_MFRC522_ReadRegister(MFRC522_REG_CRC_RESULT_L);
	pOutData[1] = TM_MFRC522_ReadRegister(MFRC522_REG_CRC_RESULT_M);
}
int tag_select(uint8_t *CardID) {
	int ret_int;
//	debug_info(
//			"Card detected    0x%02X 0x%02X 0x%02X 0x%02X, Check Sum = 0x%02X\r\n",
//			CardID[0], CardID[1], CardID[2], CardID[3], CardID[4]);
	ret_int = TM_MFRC522_SelectTag(CardID);
	if (ret_int == 0) {
		debug_info("Card Select Failed\r\n");
		return -1;
	} else {
//		debug_info("Card Selected, Type:%s\r\n",TM_MFRC522_TypeToString(TM_MFRC522_ParseType(ret_int)));
	}
	ret_int = 0;
	return ret_int;
}
//SELECTAG OGRINAL
uint8_t TM_MFRC522_SelectTag(uint8_t* serNum) {
	uint8_t i;
	TM_MFRC522_Status_t status;
	uint8_t size;
	uint16_t recvBits;
	uint8_t buffer[9]; 

	buffer[0] = PICC_SElECTTAG;
	buffer[1] = 0x70;
	for (i = 0; i < 5; i++) {
		buffer[i+2] = *(serNum+i);
	}
	TM_MFRC522_CalculateCRC(buffer, 7, &buffer[7]);		//??
	status = TM_MFRC522_ToCard(PCD_TRANSCEIVE, buffer, 9, buffer, &recvBits);
//        debug_info("status:%x - %d",status, buffer[0]);
	if ((status == MI_OK) && (recvBits == 0x18)) {   
		size = buffer[0]; 
	} else {   
		size = 0;    
	}

	return size;
}
//blob version
uint8_t _TM_MFRC522_SelectTag(uint8_t* serNum) {
	uint8_t i;
	TM_MFRC522_Status_t status;
	uint8_t size;
	uint16_t recvBits;
	uint8_t buffer[9];
	uint8_t sak[3] = {0};

	buffer[0] = PICC_SElECTTAG;
	buffer[1] = 0x70;
	for (i = 0; i < 4; i++) {
		buffer[i+2] = *(serNum+i);
	}
	buffer[6] = buffer[2] ^ buffer[3] ^ buffer[4] ^ buffer[5]; // Calculate BCC - Block Check Character
	TM_MFRC522_CalculateCRC(buffer, 7, &buffer[7]);		//??

	if (status != MI_OK) {
		//LCD_UsrLog ((char *)"Calculate crc nicht gut.\n");
		return status;
	}

	status = TM_MFRC522_ToCard(PCD_TRANSCEIVE, buffer, 9, sak, &recvBits);

	if (status != MI_OK) {
		//LCD_UsrLog ((char *)"Transceive for select tag didnt go so well.\n");
	}

	if ((status == MI_OK) && (recvBits == 0x18)) {
		size = buffer[0];
	} else {
		size = 0;
	}
	return size;
}

TM_MFRC522_Status_t TM_MFRC522_Auth(uint8_t authMode, uint8_t BlockAddr, uint8_t* Sectorkey, uint8_t* serNum) {
	TM_MFRC522_Status_t status;
	uint16_t recvBits;
	uint8_t i;
	uint8_t buff[12]; 

	//Verify the command block address + sector + password + card serial number
	buff[0] = authMode;
	buff[1] = BlockAddr;
	for (i = 0; i < 6; i++) {    
		buff[i+2] = *(Sectorkey+i);   
	}
	for (i=0; i<4; i++) {    
		buff[i+8] = *(serNum+i);   
	}
	status = TM_MFRC522_ToCard(PCD_AUTHENT, buff, 12, buff, &recvBits);

	if ((status != MI_OK) || (!(TM_MFRC522_ReadRegister(MFRC522_REG_STATUS2) & 0x08))) {   
		status = MI_ERR;   
	}

	return status;
}

TM_MFRC522_Status_t TM_MFRC522_Read(uint8_t blockAddr, uint8_t* recvData) {
	TM_MFRC522_Status_t status;
	uint16_t unLen;

	recvData[0] = PICC_READ;
	recvData[1] = blockAddr;
	TM_MFRC522_CalculateCRC(recvData,2, &recvData[2]);
	status = TM_MFRC522_ToCard(PCD_TRANSCEIVE, recvData, 4, recvData, &unLen);

	if ((status != MI_OK) || (unLen != 0x90)) {
		status = MI_ERR;
	}

	return status;
}

TM_MFRC522_Status_t TM_MFRC522_Write(uint8_t blockAddr, uint8_t* writeData) {
	TM_MFRC522_Status_t status;
	uint16_t recvBits;
	uint8_t i;
	uint8_t buff[18]; 

	buff[0] = PICC_WRITE;
	buff[1] = blockAddr;
	TM_MFRC522_CalculateCRC(buff, 2, &buff[2]);
	status = TM_MFRC522_ToCard(PCD_TRANSCEIVE, buff, 4, buff, &recvBits);

	if ((status != MI_OK) || (recvBits != 4) || ((buff[0] & 0x0F) != 0x0A)) {   
		status = MI_ERR;   
	}

	if (status == MI_OK) {
		//Data to the FIFO write 16Byte
		for (i = 0; i < 16; i++) {    
			buff[i] = *(writeData+i);   
		}
		TM_MFRC522_CalculateCRC(buff, 16, &buff[16]);
		status = TM_MFRC522_ToCard(PCD_TRANSCEIVE, buff, 18, buff, &recvBits);

		if ((status != MI_OK) || (recvBits != 4) || ((buff[0] & 0x0F) != 0x0A)) {   
			status = MI_ERR;   
		}
	}

	return status;
}

void TM_MFRC522_Halt(void) {
	uint16_t unLen;
	uint8_t buff[4]; 

	buff[0] = PICC_HALT;
	buff[1] = 0;
	TM_MFRC522_CalculateCRC(buff, 2, &buff[2]);

	TM_MFRC522_ToCard(PCD_TRANSCEIVE, buff, 4, buff, &unLen);
}

void TM_MFRC522_WakeUp(void){
	uint16_t unLen;
	uint8_t buff[4];

	buff[0] = PICC_HALT;
	buff[1] = 0;
	TM_MFRC522_CalculateCRC(buff, 2, &buff[2]);

	TM_MFRC522_ToCard(PCD_TRANSCEIVE, buff, 4, buff, &unLen);
}

char *PICC_TYPE_STRING[] = { "PICC_TYPE_NOT_COMPLETE", "PICC_TYPE_MIFARE_MINI",
		"PICC_TYPE_MIFARE_1K", "PICC_TYPE_MIFARE_4K", "PICC_TYPE_MIFARE_UL",
		"PICC_TYPE_MIFARE_PLUS", "PICC_TYPE_TNP3XXX", "PICC_TYPE_ISO_14443_4",
		"PICC_TYPE_ISO_18092", "PICC_TYPE_UNKNOWN" };
char *TM_MFRC522_TypeToString(PICC_TYPE_t type) {
	return PICC_TYPE_STRING[type];
}
int TM_MFRC522_ParseType(uint8_t TagSelectRet) {
	if (TagSelectRet & 0x04) { // UID not complete
		return PICC_TYPE_NOT_COMPLETE;
	}

	switch (TagSelectRet) {
	case 0x09:
		return PICC_TYPE_MIFARE_MINI;
		break;
	case 0x08:
		return PICC_TYPE_MIFARE_1K;
		break;
	case 0x18:
		return PICC_TYPE_MIFARE_4K;
		break;
	case 0x00:
		return PICC_TYPE_MIFARE_UL;
		break;
	case 0x10:
	case 0x11:
		return PICC_TYPE_MIFARE_PLUS;
		break;
	case 0x01:
		return PICC_TYPE_TNP3XXX;
		break;
	default:
		break;
	}

	if (TagSelectRet & 0x20) {
		return PICC_TYPE_ISO_14443_4;
	}

	if (TagSelectRet & 0x40) {
		return PICC_TYPE_ISO_18092;
	}

	return PICC_TYPE_UNKNOWN;
}
void PCD_StopCrypto1() {
	// Clear MFCrypto1On bit
	TM_MFRC522_ClearBitMask(MFRC522_REG_STATUS2, 0x08); // Status2Reg[7..0] bits are: TempSensClear I2CForceHS reserved reserved MFCrypto1On ModemState[2:0]
} // End PCD_StopCrypto1()


//THU nghiem selecteg cua thu viewn v1
