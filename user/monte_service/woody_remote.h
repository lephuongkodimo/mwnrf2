#ifndef WOODY_REMOTE_H_
#define WOODY_REMOTE_H_

#include "ble_srv_common.h"
#include "nrf_ble_gq.h"

typedef void (* monte_blocks_c_handler_t)(ble_monte_blocks_c_evt_t const * p_ble_monte_blocks_evt);
/**@brief NUS Client initialization structure. */
typedef struct
{
    monte_blocks_c_handler_t   evt_handler;    /**< Application event handler to be called when there is an event related to the NUS. */
    nrf_ble_gq_t            * p_gatt_queue;   /**< Pointer to BLE GATT Queue instance. */
    ble_srv_error_handler_t   error_handler;  /**< Function to be called in case of an error. */
} monte_blocks_c_init_t;
void monte_blocks_c_init(monte_blocks_c_init_t* monte_blocks_c_init);
//send cmd
void monte_cmd_send(char* text,uint16_t len);
//khoi tao discovery db 
void monte_disc_evt(ble_db_discovery_evt_t * p_evt);
//check Is device specfic connected?
bool monte_check_connection();
#endif